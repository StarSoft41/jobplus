<?php

namespace Ibw\JobMBundle\Command;
 
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Ibw\JobMBundle\Entity\User;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;

class JobMUsersCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('ibw:jobm:users')
            ->setDescription('Add JobM users')
            ->addArgument('username', InputArgument::REQUIRED, 'The username')
            ->addArgument('password', InputArgument::REQUIRED, 'The password')
            ->addArgument('email', InputArgument::OPTIONAL, 'email');
        ;
    }
 
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $username = $input->getArgument('username');
        $password = $input->getArgument('password');
        $email = $input->getParameterOption('email','example@mail.com');
        /** @var EntityManager $em */
        $em = $this->getContainer()->get('doctrine')->getManager();
 
        $user = new User();
        $user->setUsername($username);
        $user->setEmail($email);
        $user->setPhone(0);
        /** @var EncoderFactory $factory */
        $factory = $this->getContainer()->get('security.encoder_factory');
        $encoder = $factory->getEncoder($user);
        $encodedPassword = $encoder->encodePassword($password, $user->getSalt());
        $user->setPassword($encodedPassword);
        $em->persist($user);
        $em->flush();
 
        $output->writeln(sprintf('Added %s user with password %s', $username, $password));
    }
}