<?php

namespace Ibw\JobMBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CvType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('other_information_work', 'textarea',array(
                'required' => false,
                'label'  => 'form.other_information_work',
                'data' => '<ul><li>row 1</li><li>row 2</li><li>row 3</li></ul>'
            ))
            ->add('other_information_work_new', 'textarea',array(
                'required' => false,
                'label'  => 'form.other_information_work_new'
            ))
            ->add('qualifications', 'textarea', array(
                'required' => false,
                'label'  => 'form.qualifications'
            ))
            ->add('other_information_education', 'textarea', array(
                'required' => false,
                'label'  => 'form.other_information_education',
                'data' => '<ul><li>row 1</li><li>row 2</li><li>row 3</li></ul>'
            ))
            ->add('other_information_education_new', 'textarea', array(
                'required' => false,
                'label'  => 'form.other_information_education_new',
            ))
            ->add('skills', 'textarea', array(
                'required' => false,
                'label'  => 'form.skills'
            ))
            ->add('competence', 'textarea', array(
                'required' => false,
                'label'  => 'form.competence'
            ))
            ->add('file', 'file', array(
                'label' => 'form.file',
                'required' => false
            ))
            ->add('city',null, array(
                'required' => false,
            ))
            ->add('name', 'text', array(
//                'required' => false,
                'label' => 'form.name'
            ))
            ->add('cv_name','hidden', array(
                'label' => 'form.cv_name'
            ))
            ->add('websites','text', array(
                'required' => false,
                'label' => 'form.websites'
            ))
            ->add('street','text', array(
                'required' => false,
                'label' => 'form.street'
            ))
            ->add('country','text', array(
                'required' => false,
                'label' => 'form.country'
            ))
            ->add('nationality','text', array(
                'required' => false,
                'label' => 'form.nationality'
            ))
            ->add('job_title','text', array(
                'required' => false,
                'label' => 'form.job_title'
            ))
            ->add('company_name','text', array(
                'required' => false,
                'label' => 'form.company_name'
            ))
            ->add('start_date_work','text', array(
                'required' => false,
                'label' => 'form.start_date_work'
            ))
            ->add('end_date_work','text', array(
                'required' => false,
                'label' => 'form.end_date_work'
            ))
            ->add('job_title_new','text', array(
                'required' => false,
                'label' => 'form.job_title_new'
            ))
            ->add('company_name_new','text', array(
                'required' => false,
                'label' => 'form.company_name_new'
            ))
            ->add('start_date_work_new','text', array(
                'required' => false,
                'label' => 'form.start_date_work_new'
            ))
            ->add('end_date_work_new','text', array(
                'required' => false,
                'label' => 'form.end_date_work_new'
            ))
            ->add('specialization','text', array(
                'required' => false,
                'label' => 'form.specialization'
            ))
            ->add('institution_name','text', array(
                'required' => false,
                'label' => 'form.institution_name'
            ))
            ->add('start_date_learn','text', array(
                'required' => false,
                'label' => 'form.start_date_learn'
            ))
            ->add('end_date_learn','text', array(
                'required' => false,
                'label' => 'form.end_date_learn'
            ))
            ->add('specialization_new','text', array(
                'required' => false,
                'label' => 'form.specialization_new'
            ))
            ->add('institution_name_new','text', array(
                'required' => false,
                'label' => 'form.institution_name_new'
            ))
            ->add('start_date_learn_new','text', array(
                'required' => false,
                'label' => 'form.start_date_learn_new'
            ))
            ->add('end_date_learn_new','text', array(
                'required' => false,
                'label' => 'form.end_date_learn_new'
            ))
            ->add('phone','text', array(
                'required' => false,
                'label' => 'form.phone'
            ))
            ->add('email', 'email',array(
                'required' => false,
                'label' => 'form.email'
            ))
            ->add('new_section_1', 'textarea', array(
                'required' => false,
                'label' => 'form.new_section_1'
            ))
            ->add('new_section_2', 'textarea', array(
                'required' => false,
                'label' => 'form.new_section_2'
            ))
            ->add('new_section_3', 'textarea', array(
                'required' => false,
                'label' => 'form.new_section_3'
            ))
            ->add('language', 'hidden', array(
                'label' => 'form.language'
            ))
            ->add('new_section_1_name', 'hidden', array(
                'label' => 'form.new_section_1_name'
            ))
            ->add('new_section_2_name', 'hidden', array(
                'label' => 'form.new_section_2_name'
            ))
            ->add('new_section_3_name', 'hidden', array(
                'label' => 'form.new_section_3_name'
            ))

        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ibw\JobMBundle\Entity\Cv',
        ));
    }


    public function getName()
    {
        return 'cv';
    }

}



