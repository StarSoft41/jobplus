<?php

namespace Ibw\JobMBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CandidateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('candidate_name','text', array(
                'required' => false,
            ))
            ->add('candidate_email','text', array(
                'required' => false,
            ))
            ->add('candidate_logo','text', array(
                'required' => false,
            ))
            ->add('candidate_country','text', array(
                'required' => false,
            ))
            ->add('candidate_city','text', array(
                'required' => false,
            ))
            ->add('candidate_year_of_birth','text', array(
                'required' => false,
            ))
            ->add('candidate_description','text', array(
                'required' => false,
            ))
            ->add('candidate_street','text', array(
                'required' => false,
            ))
            ->add('candidate_gender','text', array(
                'required' => false,
            ))
            ->add('candidate_language_spoken','text', array(
                'required' => false,
            ))
            ->add('candidate_street_number','text', array(
                'required' => false,
            ))
            ->add('file', 'file', array(
                'label' => 'form.file_candidate',
                'required' => false,

            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ibw\JobMBundle\Entity\Candidate'
        ));
    }

    public function getName()
    {
        return 'candidate';
    }
}