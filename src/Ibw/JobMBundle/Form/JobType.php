<?php

namespace Ibw\JobMBundle\Form;
 
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Ibw\JobMBundle\Entity\Job;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Ibw\JobMBundle\Entity\City;
use Doctrine\ORM\EntityRepository;

class JobType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('company_name','text', array(
                'required' => false,
                'label' => 'form.company_name'
            ))
            ->add('foundation_year','text', array(
                'required' => false,
                'label' => 'form.foundation_year'
            ))
            ->add('number_of_employees','text', array(
                'required' => false,
                'label' => 'form.number_of_employees'
            ))
            ->add('job_type', 'choice', array(
                'choices'   => array(
                    'FULL-TIME'   => 'Full-time',
                    'PART-TIME' => 'Part-time',
                    'FREELANCE'   => 'Freelance',
                )))
            ->add('payment_job', 'choice', array(
                'choices'   => array(
                    'Premium'   => 'Premium',
                    'Basic' => 'Basic',
                    'Standard'   => 'Standard',
                )))
            ->add('position_role', 'text',array(
                'required' => false,
                'label'  => 'form.position_role'
            ))
            ->add('main_accountabilities', 'textarea',array(
                'required' => false,
                'label'  => 'form.main_accountabilities',
                'data' => '<ul><li>row 1</li><li>row 2</li><li>row 3</li></ul>'
            ))
            ->add('requirements', 'textarea', array(
                'required' => false,
                'label'  => 'form.requirements',
                'data' => '<ul><li>row 1</li><li>row 2</li><li>row 3</li></ul>'
            ))
            ->add('company_offer','text', array(
                'required' => false,
                'label' => 'form.company_offer'
            ))
            ->add('contacts_and_deadlines','text', array(
                'required' => false,
                'label' => 'form.contacts_and_deadlines'
            ))
            ->add('city',null, array(
                'required' => false,
            ))
            ->add('category',null, array(
                'required' => false,
            ))
            ->add('file', 'file', array(
                'label' => 'form.file',
                'required' => false

            ))
            ->add('new_section_1', 'textarea', array(
                'required' => false,
                'label' => 'form.new_section_1'
            ))
            ->add('new_section_2', 'textarea', array(
                'required' => false,
                'label' => 'form.new_section_2'
            ))
            ->add('new_section_3', 'textarea', array(
                'required' => false,
                'label' => 'form.new_section_3'
            ))
            ->add('new_section_1_name', 'hidden', array(
                'label' => 'form.new_section_1_name'
            ))
            ->add('new_section_2_name', 'hidden', array(
                'label' => 'form.new_section_2_name'
            ))
            ->add('new_section_3_name', 'hidden', array(
                'label' => 'form.new_section_3_name'
            ))
        ;
    }
 
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ibw\JobMBundle\Entity\Job'
        ));
    }
 
    public function getName()
    {
        return 'job';
    }
}