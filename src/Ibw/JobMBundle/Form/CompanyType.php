<?php

namespace Ibw\JobMBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Ibw\JobMBundle\Entity\Company;

class CompanyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('company_name','text', array(
                'required' => false,
                'label' => 'form.company_name'
            ))
            ->add('company_email','text', array(
                'required' => false,
                'label' => 'form.company_email'
            ))
            ->add('company_logo','text', array(
                'required' => false,
                'label' => 'form.company_logo'
            ))
            ->add('company_country', 'text', array(
                  'required' => false,
                  'label' => 'form.company_country'
                ))
            ->add('company_city', 'text',array(
                'required' => false,
                'label'  => 'form.company_city'
            ))

            ->add('foundation_year','text', array(
                'required' => false,
                'label' => 'form.foundation_year'
            ))
            ->add('company_description','text', array(
                'required' => false,
                'label' => 'form.company_description'
            ))
            ->add('number_of_employees','text', array(
                'required' => false,
                'label' => 'form.number_of_employees'
            ))
            ->add('company_industry','text', array(
                'required' => false,
                'label' => 'form.company_industry'
            ))
            ->add('company_street', 'text', array(
                'label' => 'form.company_street',
                'required' => false
            ))
            ->add('company_street_number', 'text', array(
                'label' => 'form.company_street_number',
                'required' => false
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ibw\JobMBundle\Entity\Company'
        ));
    }

    public function getName()
    {
        return 'company';
    }
}