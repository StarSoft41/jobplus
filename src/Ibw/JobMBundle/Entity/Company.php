<?php

namespace Ibw\JobMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;


/**
 * Company
 */
class Company extends MessageDigestPasswordEncoder
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $company_name;

    /**
     * @var string
     */
    private $company_email;

    /**
     * @var string
     */
    private $company_description;

    /**
     * @var string
     */
    private $company_logo;

    /**
     * @var string
     */
    private $company_country;

    /**
     * @var string
     */
    private $company_city;

    /**
     * @var integer
     */
    private $foundation_year;

    /**
     * @var integer
     */
    private $number_of_employees;


    /**
     * @var string
     */
    private $company_industry;

    /**
     * @var string
     */
    private $company_street;

    /**
     * @var string
     */
    private $company_street_number;


    /**
     * @var \DateTime
     */
    private $register_date;


    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $jobs;

    public function __construct()
    {
        $this->roles = new \Doctrine\Common\Collections\ArrayCollection();
        $this->register_date = new \DateTime();
        $this->jobs =  new ArrayCollection();

    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set company_name
     *
     * @param string $company_name
     * @return Company
     */
    public function setCompanyName($company_name)
    {
        $this->company_name = $company_name;

        return $this;
    }

    /**
     * Get company_name
     *
     * @return string
     */
    public function getCompanyName()
    {
        return $this->company_name;
    }


    /**
     * Set company_email
     *
     * @param string $company_email
     * @return Company
     */
    public function setCompanyEmail($company_email)
    {
        $this->company_email = $company_email;

        return $this;
    }

    /**
     * Get company_email
     *
     * @return string
     */
    public function getCompanyEmail()
    {
        return $this->company_email;
    }




    /**
     * Add roles
     *
     * @param \Ibw\JobMBundle\Entity\Role $roles
     * @return Company
     */
    public function addRole(\Ibw\JobMBundle\Entity\Role $roles)
    {
        $this->roles[] = $roles;

        return $this;
    }

    /**
     * Remove roles
     *
     * @param \Ibw\JobMBundle\Entity\Role $roles
     */
    public function removeRole(\Ibw\JobMBundle\Entity\Role $roles)
    {
        $this->roles->removeElement($roles);
    }

    /**
     * Get roles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRoles()
    {
        return $this->roles->toArray();
    }



    /**
     * Set register_date
     *
     * @param \DateTime $registerDate
     * @return Company
     */
    public function setRegisterDate($registerDate)
    {
        $this->register_date = $registerDate;

        return $this;
    }

    /**
     * Get register_date
     *
     * @return \DateTime
     */
    public function getRegisterDate()
    {
        return $this->register_date;
    }




    /**
     * Set company_logo
     *
     * @param string $companyLogo
     * @return Company
     */
    public function setCompanyLogo($companyLogo)
    {
        $this->company_logo = $companyLogo;
    
        return $this;
    }

    /**
     * Get company_logo
     *
     * @return string 
     */
    public function getCompanyLogo()
    {
        return $this->company_logo;
    }

    /**
     * Set company_country
     *
     * @param string $companyCountry
     * @return Company
     */
    public function setCompanyCountry($companyCountry)
    {
        $this->company_country = $companyCountry;
    
        return $this;
    }

    /**
     * Get company_country
     *
     * @return string 
     */
    public function getCompanyCountry()
    {
        return $this->company_country;
    }

    /**
     * Set company_city
     *
     * @param string $companyCity
     * @return Company
     */
    public function setCompanyCity($companyCity)
    {
        $this->company_city = $companyCity;
    
        return $this;
    }

    /**
     * Get company_city
     *
     * @return string 
     */
    public function getCompanyCity()
    {
        return $this->company_city;
    }

    /**
     * Set foundation_year
     *
     * @param integer $foundationYear
     * @return Company
     */
    public function setFoundationYear($foundationYear)
    {
        $this->foundation_year = $foundationYear;
    
        return $this;
    }

    /**
     * Get foundation_year
     *
     * @return integer 
     */
    public function getFoundationYear()
    {
        return $this->foundation_year;
    }

    /**
     * Set number_of_employees
     *
     * @param integer $numberOfEmployees
     * @return Company
     */
    public function setNumberOfEmployees($numberOfEmployees)
    {
        $this->number_of_employees = $numberOfEmployees;
    
        return $this;
    }

    /**
     * Get number_of_employees
     *
     * @return integer 
     */
    public function getNumberOfEmployees()
    {
        return $this->number_of_employees;
    }


    /**
     * Set company_industry
     *
     * @param string $companyIndustry
     * @return Company
     */
    public function setCompanyIndustry($companyIndustry)
    {
        $this->company_industry = $companyIndustry;
    
        return $this;
    }

    /**
     * Get company_industry
     *
     * @return string 
     */
    public function getCompanyIndustry()
    {
        return $this->company_industry;
    }

    /**
     * Set company_street
     *
     * @param string $companyStreet
     * @return Company
     */
    public function setCompanyStreet($companyStreet)
    {
        $this->company_street = $companyStreet;
    
        return $this;
    }

    /**
     * Get company_street
     *
     * @return string 
     */
    public function getCompanyStreet()
    {
        return $this->company_street;
    }

    /**
     * Set company_street_number
     *
     * @param string $companyStreetNumber
     * @return Company
     */
    public function setCompanyStreetNumber($companyStreetNumber)
    {
        $this->company_street_number = $companyStreetNumber;
    
        return $this;
    }

    /**
     * Get company_street_number
     *
     * @return string 
     */
    public function getCompanyStreetNumber()
    {
        return $this->company_street_number;
    }



    /**
     * Set company_description
     *
     * @param string $companyDescription
     * @return Company
     */
    public function setCompanyDescription($companyDescription)
    {
        $this->company_description = $companyDescription;
    
        return $this;
    }

    /**
     * Get company_description
     *
     * @return string 
     */
    public function getCompanyDescription()
    {
        return $this->company_description;
    }

    public function __toString()
    {
        return $this->company_name;
    }

    /**
     * Add jobs
     *
     * @param \Ibw\JobMBundle\Entity\Job $jobs
     * @return Company
     */
    public function addJob(\Ibw\JobMBundle\Entity\Job $jobs)
    {
        $this->jobs[] = $jobs;

        return $this;
    }

    /**
     * Remove jobs
     *
     * @param \Ibw\JobMBundle\Entity\Job $jobs
     */
    public function removeJob(\Ibw\JobMBundle\Entity\Job $jobs)
    {
        $this->jobs->removeElement($jobs);
    }

    /**
     * Get jobs
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getJobs()
    {
        return $this->jobs;
    }
    /**
     * @var \Ibw\JobMBundle\Entity\User
     */
    private $user;


    /**
     * Set user
     *
     * @param \Ibw\JobMBundle\Entity\User $user
     * @return Company
     */
    public function setUser(\Ibw\JobMBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Ibw\JobMBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
