<?php

namespace Ibw\JobMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Candidate
 */
class Candidate
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $candidate_name;

    /**
     * @var string
     */
    private $candidate_email;

    /**
     * @var string
     */
    private $candidate_logo;

    /**
     * @var string
     */
    private $candidate_country;

    /**
     * @var string
     */
    private $candidate_city;

    /**
     * @var string
     */
    private $candidate_year_of_birth;

    /**
     * @var string
     */
    private $candidate_description;

    /**
     * @var string
     */
    private $candidate_street;

    /**
     * @var string
     */
    private $candidate_gender;

    /**
     * @var string
     */
    private $candidate_language_spoken;

    /**
     * @var string
     */
    private $candidate_street_number;

    /**
     * @var \DateTime
     */
    private $register_date;

    /**
     * @var \DateTime
     */
    private $updated_at;

    public $file;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $image;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $cv;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cv = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set candidate_name
     *
     * @param string $candidateName
     * @return Candidate
     */
    public function setCandidateName($candidateName)
    {
        $this->candidate_name = $candidateName;

        return $this;
    }

    /**
     * Get candidate_name
     *
     * @return string 
     */
    public function getCandidateName()
    {
        return $this->candidate_name;
    }

    /**
     * Set candidate_email
     *
     * @param string $candidateEmail
     * @return Candidate
     */
    public function setCandidateEmail($candidateEmail)
    {
        $this->candidate_email = $candidateEmail;

        return $this;
    }

    /**
     * Get candidate_email
     *
     * @return string 
     */
    public function getCandidateEmail()
    {
        return $this->candidate_email;
    }

    /**
     * Set candidate_logo
     *
     * @param string $candidateLogo
     * @return Candidate
     */
    public function setCandidateLogo($candidateLogo)
    {
        $this->candidate_logo = $candidateLogo;

        return $this;
    }

    /**
     * Get candidate_logo
     *
     * @return string 
     */
    public function getCandidateLogo()
    {
        return $this->candidate_logo;
    }

    /**
     * Set candidate_country
     *
     * @param string $candidateCountry
     * @return Candidate
     */
    public function setCandidateCountry($candidateCountry)
    {
        $this->candidate_country = $candidateCountry;

        return $this;
    }

    /**
     * Get candidate_country
     *
     * @return string 
     */
    public function getCandidateCountry()
    {
        return $this->candidate_country;
    }

    /**
     * Set candidate_city
     *
     * @param string $candidateCity
     * @return Candidate
     */
    public function setCandidateCity($candidateCity)
    {
        $this->candidate_city = $candidateCity;

        return $this;
    }

    /**
     * Get candidate_city
     *
     * @return string 
     */
    public function getCandidateCity()
    {
        return $this->candidate_city;
    }

    /**
     * Set candidate_year_of_birth
     *
     * @param string $candidateYearOfBirth
     * @return Candidate
     */
    public function setCandidateYearOfBirth($candidateYearOfBirth)
    {
        $this->candidate_year_of_birth = $candidateYearOfBirth;

        return $this;
    }

    /**
     * Get candidate_year_of_birth
     *
     * @return string 
     */
    public function getCandidateYearOfBirth()
    {
        return $this->candidate_year_of_birth;
    }

    /**
     * Set candidate_description
     *
     * @param string $candidateDescription
     * @return Candidate
     */
    public function setCandidateDescription($candidateDescription)
    {
        $this->candidate_description = $candidateDescription;

        return $this;
    }

    /**
     * Get candidate_description
     *
     * @return string 
     */
    public function getCandidateDescription()
    {
        return $this->candidate_description;
    }

    /**
     * Set candidate_street
     *
     * @param string $candidateStreet
     * @return Candidate
     */
    public function setCandidateStreet($candidateStreet)
    {
        $this->candidate_street = $candidateStreet;

        return $this;
    }

    /**
     * Get candidate_street
     *
     * @return string 
     */
    public function getCandidateStreet()
    {
        return $this->candidate_street;
    }

    /**
     * Set candidate_gender
     *
     * @param string $candidateGender
     * @return Candidate
     */
    public function setCandidateGender($candidateGender)
    {
        $this->candidate_gender = $candidateGender;

        return $this;
    }

    /**
     * Get candidate_gender
     *
     * @return string 
     */
    public function getCandidateGender()
    {
        return $this->candidate_gender;
    }

    /**
     * Set candidate_language_spoken
     *
     * @param string $candidateLanguageSpoken
     * @return Candidate
     */
    public function setCandidateLanguageSpoken($candidateLanguageSpoken)
    {
        $this->candidate_language_spoken = $candidateLanguageSpoken;

        return $this;
    }

    /**
     * Get candidate_language_spoken
     *
     * @return string 
     */
    public function getCandidateLanguageSpoken()
    {
        return $this->candidate_language_spoken;
    }

    /**
     * Set candidate_street_number
     *
     * @param string $candidateStreetNumber
     * @return Candidate
     */
    public function setCandidateStreetNumber($candidateStreetNumber)
    {
        $this->candidate_street_number = $candidateStreetNumber;

        return $this;
    }

    /**
     * Get candidate_street_number
     *
     * @return string 
     */
    public function getCandidateStreetNumber()
    {
        return $this->candidate_street_number;
    }

    /**
     * Set register_date
     *
     * @param \DateTime $registerDate
     * @return Candidate
     */
    public function setRegisterDate($registerDate)
    {
        $this->register_date = $registerDate;

        return $this;
    }

    /**
     * Get register_date
     *
     * @return \DateTime 
     */
    public function getRegisterDate()
    {
        return $this->register_date;
    }

    public function setRegisterAtValue()
    {
        if(!$this->getRegisterDate()) {
            $this->register_date = new \DateTime();
        }
    }

    /**
     * Add cv
     *
     * @param \Ibw\JobMBundle\Entity\Cv $cv
     * @return Candidate
     */
    public function addCv(\Ibw\JobMBundle\Entity\Cv $cv)
    {
        $this->cv[] = $cv;

        return $this;
    }

    /**
     * Remove cv
     *
     * @param \Ibw\JobMBundle\Entity\Cv $cv
     */
    public function removeCv(\Ibw\JobMBundle\Entity\Cv $cv)
    {
        $this->cv->removeElement($cv);
    }

    /**
     * Get cv
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCv()
    {
        return $this->cv;
    }
    /**
     * @var \Ibw\JobMBundle\Entity\User
     */
    private $user;


    /**
     * Set user
     *
     * @param \Ibw\JobMBundle\Entity\User $user
     * @return Candidate
     */
    public function setUser(\Ibw\JobMBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Ibw\JobMBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @ORM\PrePersist
     */
    public function preUpload()
    {
        if (null !== $this->file) {
            $this->candidate_logo = uniqid() . '.' . $this->file->guessExtension();
        }
    }

    /**
     * @ORM\PostPersist
     */
    public function upload()
    {
        if (null === $this->file) {
            return;
        }

        // If there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->file->move($this->getUploadRootDir(), $this->candidate_logo);

        unset($this->file);
    }

    public function getUploadRootDirImg()
    {
        // absolute path to your directory where images must be saved
        return __DIR__.'/../../../../../web/'.$this->getUploadDir();
    }

    public function getUploadDirImg()
    {
        return 'uploads/myentitycandidate';
    }

    public function getAbsolutePathImg()
    {
        return null === $this->image ? null : $this->getUploadRootDir().'/'.$this->image;
    }

    public function getWebPathImg()
    {
        return null === $this->image ? null : '/'.$this->getUploadDir().'/'.$this->image;
    }

    protected function getUploadDir()
    {
        return 'uploads/candidate';
    }

    protected function getUploadRootDir()
    {
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    public function getWebPath()
    {
        return null === $this->candidate_logo ? null : $this->getUploadDir() . '/' . $this->candidate_logo;
    }

    public function getAbsolutePath()
    {
        return null === $this->candidate_logo ? null : $this->getUploadRootDir() . '/' . $this->candidate_logo;
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdatedAtValue()
    {
        $this->updated_at = new \DateTime();
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return Candidate
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    public function asArray($host)
    {
        return array(
            'candidate_name' => $this->getCandidateName(),
            'logo' => $this->getCandidateLogo() ? 'http://' . $host . '/uploads/candidate/' . $this->getCandidateLogo() : null,
            'candidate_city' => $this->getCandidateCity(),
        );
    }
}
