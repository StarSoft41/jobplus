<?php

namespace Ibw\JobMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MessagesChat
 */
class MessagesChat
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $messages;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $users;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set messages
     *
     * @param string $messages
     * @return MessagesChat
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;

        return $this;
    }

    /**
     * Get messages
     *
     * @return string 
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * Add users
     *
     * @param \Ibw\JobMBundle\Entity\User $users
     * @return MessagesChat
     */
    public function addUser(\Ibw\JobMBundle\Entity\User $users)
    {
        $this->users[] = $users;

        return $this;
    }

    /**
     * Remove users
     *
     * @param \Ibw\JobMBundle\Entity\User $users
     */
    public function removeUser(\Ibw\JobMBundle\Entity\User $users)
    {
        $this->users->removeElement($users);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsers()
    {
        return $this->users;
    }
}
