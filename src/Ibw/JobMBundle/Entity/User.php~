<?php

namespace Ibw\JobMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use Symfony\Component\Security\Core\User\UserInterface;
use FOS\UserBundle\Entity as BaseUser;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * User
 */
class User extends MessageDigestPasswordEncoder implements UserInterface, \Serializable
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $salt;

    /**
     * @var string
     */
    private $email;


    /**
     * @var \DateTime
     */
    private $register_date;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $roles;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $cv;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $jobs;

    /**
     * @var string
     */
    private $select;

    /**
     * @var string
     *
     * @ORM\Column(name="facebook_id", type="string", nullable=true)
     */
    private $facebookId;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->roles = new \Doctrine\Common\Collections\ArrayCollection();
        $this->register_date = new \DateTime();
        $this->salt = md5(time());
        $this->cv = new ArrayCollection();
        $this->jobs = new ArrayCollection();
        $this->favorits = new ArrayCollection();
        $this->messageschat = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set salt
     *
     * @param string $salt
     * @return User
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Get salt
     *
     * @return string 
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }


    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }


    /**
     * Set register_date
     *
     * @param \DateTime $registerDate
     * @return User
     */
    public function setRegisterDate($registerDate)
    {
        $this->register_date = $registerDate;

        return $this;
    }

    /**
     * Get register_date
     *
     * @return \DateTime 
     */
    public function getRegisterDate()
    {
        return $this->register_date;
    }

    /**
     * Add roles
     *
     * @param \Ibw\JobMBundle\Entity\Role $roles
     * @return User
     */
    public function addRole(\Ibw\JobMBundle\Entity\Role $roles)
    {
        $this->roles[] = $roles;

        return $this;
    }

    /**
     * Remove roles
     *
     * @param \Ibw\JobMBundle\Entity\Role $roles
     */
    public function removeRole(\Ibw\JobMBundle\Entity\Role $roles)
    {
        $this->roles->removeElement($roles);
    }

    /**
     * Get roles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRoles()
    {
        return $this->roles->toArray();
    }


    /**
     * Set select
     *
     * @param string $select
     * @return User
     */
    public function setSelect($select)
    {
        $this->select = $select;

        return $this;
    }

    /**
     * Get select
     *
     * @return string
     */
    public function getSelect()
    {
        return $this->select;
    }


    public function setFacebookId($facebookId)
    {
        $this->facebookId = $facebookId;
    }

    /**
     * @return string
     */
    public function getFacebookId()
    {
        return $this->facebookId;
    }


    public function eraseCredentials()
    {

    }

    public function serialize()
    {
        return \ json_encode ( array(
            $this->username,
            $this->password,
            $this->roles,
            $this->id
        ));
    }

    public function unserialize($serialized)
    {
        list($this->username,
            $this->password,
            $this->roles,
            $this->id
            )
            = \json_decode($serialized);
    }

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('email', new NotBlank());
        //$metadata->addPropertyConstraint('email', new Email());

        $metadata->addConstraint(new UniqueEntity(array(
                "fields" => "email",
                "message" => "This email address is already in use")
        ));
    }



    public function __toString()
    {
        return $this->username;
    }

     /**
     * @param \Doctrine\Common\Collections\Collection $cv
     * @return $this
     */
    public function setCv(Collection $cv)
    {
        $this->cv = $cv;

        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCv()
    {
        return $this->cv;
    }


    /**
     * Add cv
     *
     * @param \Ibw\JobMBundle\Entity\Cv $cv
     * @return Category
     */
    public function addCv(\Ibw\JobMBundle\Entity\Cv $cv)
    {
        $this->cv[] = $cv;

        return $this;
    }

    /**
     * Remove cv
     *
     * @param \Ibw\JobMBundle\Entity\Cv $cv
     */
    public function removeCv(\Ibw\JobMBundle\Entity\Cv $cv)
    {
        $this->cv->removeElement($cv);
    }

    /**
     * Add jobs
     *
     * @param \Ibw\JobMBundle\Entity\Job $jobs
     * @return User
     */
    public function addJob(\Ibw\JobMBundle\Entity\Job $jobs)
    {
        $this->jobs[] = $jobs;

        return $this;
    }

    /**
     * Remove jobs
     *
     * @param \Ibw\JobMBundle\Entity\Job $jobs
     */
    public function removeJob(\Ibw\JobMBundle\Entity\Job $jobs)
    {
        $this->jobs->removeElement($jobs);
    }

    /**
     * Get jobs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getJobs()
    {
        return $this->jobs;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $messageschat;

    /**
     * Add messageschat
     *
     * @param \Ibw\JobMBundle\Entity\MessagesChat $messageschat
     * @return User
     */
    public function addMessageschat(\Ibw\JobMBundle\Entity\MessagesChat $messageschat)
    {
        $this->messageschat[] = $messageschat;

        return $this;
    }

    /**
     * Remove messageschat
     *
     * @param \Ibw\JobMBundle\Entity\MessagesChat $messageschat
     */
    public function removeMessageschat(\Ibw\JobMBundle\Entity\MessagesChat $messageschat)
    {
        $this->messageschat->removeElement($messageschat);
    }

    /**
     * Get messageschat
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMessageschat()
    {
        return $this->messageschat;
    }


    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $favorits;


    /**
     * Get favorits
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFavorits()
    {
        return $this->favorits;
    }


    /**
     * Add favorits
     *
     * @param \Ibw\JobMBundle\Entity\Favorites $favorits
     * @return User
     */
    public function addFavorit(\Ibw\JobMBundle\Entity\Favorites $favorits)
    {
        $this->favorits[] = $favorits;

        return $this;
    }

    /**
     * Remove favorits
     *
     * @param \Ibw\JobMBundle\Entity\Favorites $favorits
     */
    public function removeFavorit(\Ibw\JobMBundle\Entity\Favorites $favorits)
    {
        $this->favorits->removeElement($favorits);
    }

    /**
     * Set username
     *
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }
    /**
     * @var \Ibw\JobMBundle\Entity\Candidate
     */
    private $candidate;


    /**
     * Set candidate
     *
     * @param \Ibw\JobMBundle\Entity\Candidate $candidate
     * @return User
     */
    public function setCandidate(\Ibw\JobMBundle\Entity\Candidate $candidate = null)
    {
        $this->candidate = $candidate;

        return $this;
    }

    /**
     * Get candidate
     *
     * @return \Ibw\JobMBundle\Entity\Candidate 
     */
    public function getCandidate()
    {
        return $this->candidate;
    }
}
