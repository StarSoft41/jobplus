<?php

namespace Ibw\JobMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ibw\JobMBundle\Utils\Jobeet as Jobeet;

/**
 * Job
 */
class Job
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $company_name;

    /**
     * @var string
     */
    private $foundation_year;

    /**
     * @var string
     */
    private $number_of_employees;

    /**
     * @var string
     */
    private $job_type;

    /**
     * @var string
     */
    private $position_role;

    /**
     * @var string
     */
    private $main_accountabilities;

    /**
     * @var string
     */
    private $requirements;

    /**
     * @var string
     */
    private $company_offer;

    /**
     * @var string
     */
    private $contacts_and_deadlines;

    /**
     * @var boolean
     */
    private $is_public;

    /**
     * @var \DateTime
     */
    private $created_at;

    /**
     * @var \DateTime
     */
    private $expires_at;

    /**
     * @var \DateTime
     */
    private $updated_at;

//    /**
//     * @var string
//     */
//    private $premium;
//
//    /**
//     * @var string
//     */
//    private $basic;
//
//    /**
//     * @var string
//     */
//    private $standard;

    /**
     * @var string
     */
    private $token;

    /**
     * @var string
     */
    private $logo;

    /**
     * @var \Ibw\JobMBundle\Entity\Category
     */
    private $category;

    /**
     * @var \Ibw\JobMBundle\Entity\City
     */
    private $city;

    /**
     * @var \Ibw\JobMBundle\Entity\User
     */
    private $user;

    public $file;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $image;

    /**
     * @var boolean
     */
    private $is_activated;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set company_name
     *
     * @param string $companyName
     * @return Job
     */
    public function setCompanyName($companyName)
    {
        $this->company_name = $companyName;

        return $this;
    }

    /**
     * Get company_name
     *
     * @return string 
     */
    public function getCompanyName()
    {
        return $this->company_name;
    }

    /**
     * Set foundation_year
     *
     * @param string $foundationYear
     * @return Job
     */
    public function setFoundationYear($foundationYear)
    {
        $this->foundation_year = $foundationYear;

        return $this;
    }

    /**
     * Get foundation_year
     *
     * @return string 
     */
    public function getFoundationYear()
    {
        return $this->foundation_year;
    }

    /**
     * Set number_of_employees
     *
     * @param string $numberOfEmployees
     * @return Job
     */
    public function setNumberOfEmployees($numberOfEmployees)
    {
        $this->number_of_employees = $numberOfEmployees;

        return $this;
    }

    /**
     * Get number_of_employees
     *
     * @return string 
     */
    public function getNumberOfEmployees()
    {
        return $this->number_of_employees;
    }

    /**
     * Set job_type
     *
     * @param string $jobType
     * @return Job
     */
    public function setJobType($jobType)
    {
        $this->job_type = $jobType;

        return $this;
    }

    /**
     * Get job_type
     *
     * @return string 
     */
    public function getJobType()
    {
        return $this->job_type;
    }

    /**
     * Set position_role
     *
     * @param string $positionRole
     * @return Job
     */
    public function setPositionRole($positionRole)
    {
        $this->position_role = $positionRole;

        return $this;
    }

    /**
     * Get position_role
     *
     * @return string 
     */
    public function getPositionRole()
    {
        return $this->position_role;
    }

    /**
     * Set main_accountabilities
     *
     * @param string $mainAccountabilities
     * @return Job
     */
    public function setMainAccountabilities($mainAccountabilities)
    {
        $this->main_accountabilities = $mainAccountabilities;

        return $this;
    }

    /**
     * Get main_accountabilities
     *
     * @return string 
     */
    public function getMainAccountabilities()
    {
        return $this->main_accountabilities;
    }

    /**
     * Set requirements
     *
     * @param string $requirements
     * @return Job
     */
    public function setRequirements($requirements)
    {
        $this->requirements = $requirements;

        return $this;
    }

    /**
     * Get requirements
     *
     * @return string 
     */
    public function getRequirements()
    {
        return $this->requirements;
    }

    /**
     * Set company_offer
     *
     * @param string $companyOffer
     * @return Job
     */
    public function setCompanyOffer($companyOffer)
    {
        $this->company_offer = $companyOffer;

        return $this;
    }

    /**
     * Get company_offer
     *
     * @return string 
     */
    public function getCompanyOffer()
    {
        return $this->company_offer;
    }

    /**
     * Set contacts_and_deadlines
     *
     * @param string $contactsAndDeadlines
     * @return Job
     */
    public function setContactsAndDeadlines($contactsAndDeadlines)
    {
        $this->contacts_and_deadlines = $contactsAndDeadlines;

        return $this;
    }

    /**
     * Get contacts_and_deadlines
     *
     * @return string 
     */
    public function getContactsAndDeadlines()
    {
        return $this->contacts_and_deadlines;
    }

    /**
     * Set is_public
     *
     * @param boolean $isPublic
     * @return Job
     */
    public function setIsPublic($isPublic)
    {
        $this->is_public = $isPublic;

        return $this;
    }

    /**
     * Get is_public
     *
     * @return boolean 
     */
    public function getIsPublic()
    {
        return $this->is_public;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Job
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set expires_at
     *
     * @param \DateTime $expiresAt
     * @return Job
     */
    public function setExpiresAt($expiresAt)
    {
        $this->expires_at = $expiresAt;

        return $this;
    }

    /**
     * Get expires_at
     *
     * @return \DateTime 
     */
    public function getExpiresAt()
    {
        return $this->expires_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return Job
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

//    /**
//     * Set premium
//     *
//     * @param string $premium
//     * @return Job
//     */
//    public function setPremium($premium)
//    {
//        $this->premium = $premium;
//
//        return $this;
//    }
//
//    /**
//     * Get premium
//     *
//     * @return string
//     */
//    public function getPremium()
//    {
//        return $this->premium;
//    }
//
//    /**
//     * Set basic
//     *
//     * @param string $basic
//     * @return Job
//     */
//    public function setBasic($basic)
//    {
//        $this->basic = $basic;
//
//        return $this;
//    }
//
//    /**
//     * Get basic
//     *
//     * @return string
//     */
//    public function getBasic()
//    {
//        return $this->basic;
//    }
//
//    /**
//     * Set standard
//     *
//     * @param string $standard
//     * @return Job
//     */
//    public function setStandard($standard)
//    {
//        $this->standard = $standard;
//
//        return $this;
//    }
//
//    /**
//     * Get standard
//     *
//     * @return string
//     */
//    public function getStandard()
//    {
//        return $this->standard;
//    }

    /**
     * Set token
     *
     * @param string $token
     * @return Job
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string 
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set logo
     *
     * @param string $logo
     * @return Job
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set category
     *
     * @param \Ibw\JobMBundle\Entity\Category $category
     * @return Job
     */
    public function setCategory(\Ibw\JobMBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Ibw\JobMBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set city
     *
     * @param \Ibw\JobMBundle\Entity\City $city
     * @return Job
     */
    public function setCity(\Ibw\JobMBundle\Entity\City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \Ibw\JobMBundle\Entity\City 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set user
     *
     * @param \Ibw\JobMBundle\Entity\User $user
     * @return Job
     */
    public function setUser(\Ibw\JobMBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Ibw\JobMBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
    /**
     * @ORM\PrePersist
     */
    public function setTokenValue()
    {
        if(!$this->getToken()) {
            $this->token = sha1($this->getCompanyName().rand(11111, 99999));
        }
    }

    /**
     * @ORM\PrePersist
     */
    public function preUpload()
    {
        if (null !== $this->file) {
            $this->logo = uniqid() . '.' . $this->file->guessExtension();
        }
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        if(!$this->getCreatedAt()) {
            $this->created_at = new \DateTime();
        }
    }

    /**
     * @ORM\PrePersist
     */
    public function setExpiresAtValue()
    {
        if(!$this->getExpiresAt()) {
            $now = $this->getCreatedAt() ? $this->getCreatedAt()->format('U') : time();
            $this->expires_at = new \DateTime(date('Y-m-d H:i:s', $now + 86400 * 30));
        }
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdatedAtValue()
    {
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PostPersist
     */
    public function upload()
    {
        if (null === $this->file) {
            return;
        }

        // If there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->file->move($this->getUploadRootDir(), $this->logo);

        unset($this->file);
    }

    public function getUploadRootDirImg()
    {
        // absolute path to your directory where images must be saved
        return __DIR__.'/../../../../../web/'.$this->getUploadDir();
    }

    public function getUploadDirImg()
    {
        return 'uploads/myentityjob';
    }

    public function getAbsolutePathImg()
    {
        return null === $this->image ? null : $this->getUploadRootDir().'/'.$this->image;
    }

    public function getWebPathImg()
    {
        return null === $this->image ? null : '/'.$this->getUploadDir().'/'.$this->image;
    }

    protected function getUploadDir()
    {
        return 'uploads/jobs';
    }

    protected function getUploadRootDir()
    {
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    public function getWebPath()
    {
        return null === $this->logo ? null : $this->getUploadDir() . '/' . $this->logo;
    }

    public function getAbsolutePath()
    {
        return null === $this->logo ? null : $this->getUploadRootDir() . '/' . $this->logo;
    }


    /**
     * Set is_activated
     *
     * @param boolean $isActivated
     * @return Cv
     */
    public function setIsActivated($isActivated)
    {
        $this->is_activated = $isActivated;

        return $this;
    }

    /**
     * Get is_activated
     *
     * @return boolean
     */
    public function getIsActivated()
    {
        return $this->is_activated;
    }

    public function publish()
    {
        $this->setIsActivated(true);
    }


    public function extend()
    {
        if (!$this->expiresSoon()) {
            return false;
        }

        $this->expires_at = new \DateTime(date('Y-m-d H:i:s', time() + 86400 * 30));

        return true;
    }


    public function asArray($host)
    {
        return array(
            'company_name' => $this->getCompanyName(),
            'logo' => $this->getLogo() ? 'http://' . $host . '/uploads/jobs/' . $this->getLogo() : null,
            'city' => $this->getCity(),
            'expires_at' => $this->getCreatedAt()->format('Y-m-d H:i:s'),
        );
    }

    public function isExpired()
    {
        return $this->getDaysBeforeExpires() < 0;
    }

    public function expiresSoon()
    {
        return $this->getDaysBeforeExpires() < 5;
    }

    public function getDaysBeforeExpires()
    {
        return ceil(($this->getExpiresAt()->format('U') - time()) / 86400);
    }




    /**
     * @var string
     */
    private $new_section_1;

    /**
     * @var string
     */
    private $new_section_2;

    /**
     * @var string
     */
    private $new_section_3;

    /**
     * @var string
     */
    private $new_section_1_name;

    /**
     * @var string
     */
    private $new_section_2_name;

    /**
     * @var string
     */
    private $new_section_3_name;


    /**
     * Set new_section_1
     *
     * @param string $newSection1
     * @return Job
     */
    public function setNewSection1($newSection1)
    {
        $this->new_section_1 = $newSection1;

        return $this;
    }

    /**
     * Get new_section_1
     *
     * @return string 
     */
    public function getNewSection1()
    {
        return $this->new_section_1;
    }

    /**
     * Set new_section_2
     *
     * @param string $newSection2
     * @return Job
     */
    public function setNewSection2($newSection2)
    {
        $this->new_section_2 = $newSection2;

        return $this;
    }

    /**
     * Get new_section_2
     *
     * @return string 
     */
    public function getNewSection2()
    {
        return $this->new_section_2;
    }

    /**
     * Set new_section_3
     *
     * @param string $newSection3
     * @return Job
     */
    public function setNewSection3($newSection3)
    {
        $this->new_section_3 = $newSection3;

        return $this;
    }

    /**
     * Get new_section_3
     *
     * @return string 
     */
    public function getNewSection3()
    {
        return $this->new_section_3;
    }

    /**
     * Set new_section_1_name
     *
     * @param string $newSection1Name
     * @return Job
     */
    public function setNewSection1Name($newSection1Name)
    {
        $this->new_section_1_name = $newSection1Name;

        return $this;
    }

    /**
     * Get new_section_1_name
     *
     * @return string 
     */
    public function getNewSection1Name()
    {
        return $this->new_section_1_name;
    }

    /**
     * Set new_section_2_name
     *
     * @param string $newSection2Name
     * @return Job
     */
    public function setNewSection2Name($newSection2Name)
    {
        $this->new_section_2_name = $newSection2Name;

        return $this;
    }

    /**
     * Get new_section_2_name
     *
     * @return string 
     */
    public function getNewSection2Name()
    {
        return $this->new_section_2_name;
    }

    /**
     * Set new_section_3_name
     *
     * @param string $newSection3Name
     * @return Job
     */
    public function setNewSection3Name($newSection3Name)
    {
        $this->new_section_3_name = $newSection3Name;

        return $this;
    }

    /**
     * Get new_section_3_name
     *
     * @return string 
     */
    public function getNewSection3Name()
    {
        return $this->new_section_3_name;
    }
    /**
     * @var string
     */
    private $payment_job;


    /**
     * Set payment_job
     *
     * @param string $paymentJob
     * @return Job
     */
    public function setPaymentJob($paymentJob)
    {
        $this->payment_job = $paymentJob;

        return $this;
    }

    /**
     * Get payment_job
     *
     * @return string 
     */
    public function getPaymentJob()
    {
        return $this->payment_job;
    }
    /**
     * @var \Ibw\JobMBundle\Entity\Company
     */
    private $company;


    /**
     * Set company
     *
     * @param \Ibw\JobMBundle\Entity\Company $company
     * @return Job
     */
    public function setCompany(\Ibw\JobMBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \Ibw\JobMBundle\Entity\Company 
     */
    public function getCompany()
    {
        return $this->company;
    }
}
