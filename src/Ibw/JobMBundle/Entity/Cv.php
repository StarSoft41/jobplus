<?php


namespace Ibw\JobMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ibw\JobMBundle\Utils\Jobeet as Jobeet;



/**
 * Cv
 */
class Cv
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $cv_name;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var string
     */
    private $websites;

    /**
     * @var string
     */
    private $street;

    /**
     * @var string
     */
    private $country;

    /**
     * @var string
     */
    private $logo;

    /**
     * @var string
     */
    private $job_title;

    /**
     * @var string
     */
    private $job_title_new;

    /**
     * @var string
     */
    private $company_name;

    /**
     * @var string
     */
    private $company_name_new;

    /**
     * @var string
     */
    private $start_date_work;

    /**
     * @var string
     */
    private $start_date_work_new;

    /**
     * @var string
     */
    private $end_date_work;

    /**
     * @var string
     */
    private $end_date_work_new;

    /**
     * @var string
     */
    private $other_information_work;

    /**
     * @var string
     */
    private $other_information_work_new;

    /**
     * @var string
     */
    private $qualifications;

    /**
     * @var string
     */
    private $specialization;

    /**
     * @var string
     */
    private $specialization_new;

    /**
     * @var string
     */
    private $institution_name;

    /**
     * @var string
     */
    private $institution_name_new;

    /**
     * @var string
     */
    private $start_date_learn;

    /**
     * @var string
     */
    private $start_date_learn_new;

    /**
     * @var string
     */
    private $end_date_learn;

    /**
     * @var string
     */
    private $end_date_learn_new;

    /**
     * @var string
     */
    private $other_information_education;

    /**
     * @var string
     */
    private $other_information_education_new;

    /**
     * @var string
     */
    private $skills;

    /**
     * @var string
     */
    private $competence;

    /**
     * @var string
     */
    private $new_section_1;

    /**
     * @var string
     */
    private $new_section_2;

    /**
     * @var string
     */
    private $new_section_3;

    /**
     * @var boolean
     */
    private $is_public;

    /**
     * @var \DateTime
     */
    private $expires_at;

    /**
     * @var \DateTime
     */
    private $created_at;

    /**
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @var string
     */
    private $token;

    /**
     * @var string
     */
    private $language;

    /**
     * @var string
     */
    private $new_section_1_name;

    /**
     * @var string
     */
    private $new_section_2_name;

    /**
     * @var string
     */
    private $new_section_3_name;

    /**
     * @var \Ibw\JobMBundle\Entity\User
     */
    private $user;

    /**
     * @var \Ibw\JobMBundle\Entity\City
     */
    private $city;

    public $file;

    /**
     * @var boolean
     */
    private $is_activated;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $image;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Cv
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set cv_name
     *
     * @param string $cvName
     * @return Cv
     */
    public function setCvName($cvName)
    {
        $this->cv_name = $cvName;

        return $this;
    }

    /**
     * Get cv_name
     *
     * @return string
     */
    public function getCvName()
    {
        return $this->cv_name;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Cv
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Cv
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set websites
     *
     * @param string $websites
     * @return Cv
     */
    public function setWebsites($websites)
    {
        $this->websites = $websites;

        return $this;
    }

    /**
     * Get websites
     *
     * @return string
     */
    public function getWebsites()
    {
        return $this->websites;
    }

    /**
     * Set street
     *
     * @param string $street
     * @return Cv
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return Cv
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set logo
     *
     * @param string $logo
     * @return Cv
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set job_title
     *
     * @param string $jobTitle
     * @return Cv
     */
    public function setJobTitle($jobTitle)
    {
        $this->job_title = $jobTitle;

        return $this;
    }

    /**
     * Get job_title
     *
     * @return string
     */
    public function getJobTitle()
    {
        return $this->job_title;
    }

    /**
     * Set job_title_new
     *
     * @param string $jobTitleNew
     * @return Cv
     */
    public function setJobTitleNew($jobTitleNew)
    {
        $this->job_title_new = $jobTitleNew;

        return $this;
    }

    /**
     * Get job_title_new
     *
     * @return string
     */
    public function getJobTitleNew()
    {
        return $this->job_title_new;
    }

    /**
     * Set company_name
     *
     * @param string $companyName
     * @return Cv
     */
    public function setCompanyName($companyName)
    {
        $this->company_name = $companyName;

        return $this;
    }

    /**
     * Get company_name
     *
     * @return string
     */
    public function getCompanyName()
    {
        return $this->company_name;
    }

    /**
     * Set company_name_new
     *
     * @param string $companyNameNew
     * @return Cv
     */
    public function setCompanyNameNew($companyNameNew)
    {
        $this->company_name_new = $companyNameNew;

        return $this;
    }

    /**
     * Get company_name_new
     *
     * @return string
     */
    public function getCompanyNameNew()
    {
        return $this->company_name_new;
    }

    /**
     * Set start_date_work
     *
     * @param string $startDateWork
     * @return Cv
     */
    public function setStartDateWork($startDateWork)
    {
        $this->start_date_work = $startDateWork;

        return $this;
    }

    /**
     * Get start_date_work
     *
     * @return string
     */
    public function getStartDateWork()
    {
        return $this->start_date_work;
    }

    /**
     * Set start_date_work_new
     *
     * @param string $startDateWorkNew
     * @return Cv
     */
    public function setStartDateWorkNew($startDateWorkNew)
    {
        $this->start_date_work_new = $startDateWorkNew;

        return $this;
    }

    /**
     * Get start_date_work_new
     *
     * @return string
     */
    public function getStartDateWorkNew()
    {
        return $this->start_date_work_new;
    }

    /**
     * Set end_date_work
     *
     * @param string $endDateWork
     * @return Cv
     */
    public function setEndDateWork($endDateWork)
    {
        $this->end_date_work = $endDateWork;

        return $this;
    }

    /**
     * Get end_date_work
     *
     * @return string
     */
    public function getEndDateWork()
    {
        return $this->end_date_work;
    }

    /**
     * Set end_date_work_new
     *
     * @param string $endDateWorkNew
     * @return Cv
     */
    public function setEndDateWorkNew($endDateWorkNew)
    {
        $this->end_date_work_new = $endDateWorkNew;

        return $this;
    }

    /**
     * Get end_date_work_new
     *
     * @return string
     */
    public function getEndDateWorkNew()
    {
        return $this->end_date_work_new;
    }

    /**
     * Set other_information_work
     *
     * @param string $otherInformationWork
     * @return Cv
     */
    public function setOtherInformationWork($otherInformationWork)
    {
        $this->other_information_work = $otherInformationWork;

        return $this;
    }

    /**
     * Get other_information_work
     *
     * @return string
     */
    public function getOtherInformationWork()
    {
        return $this->other_information_work;
    }

    /**
     * Set other_information_work_new
     *
     * @param string $otherInformationWorkNew
     * @return Cv
     */
    public function setOtherInformationWorkNew($otherInformationWorkNew)
    {
        $this->other_information_work_new = $otherInformationWorkNew;

        return $this;
    }

    /**
     * Get other_information_work_new
     *
     * @return string
     */
    public function getOtherInformationWorkNew()
    {
        return $this->other_information_work_new;
    }

    /**
     * Set qualifications
     *
     * @param string $qualifications
     * @return Cv
     */
    public function setQualifications($qualifications)
    {
        $this->qualifications = $qualifications;

        return $this;
    }

    /**
     * Get qualifications
     *
     * @return string
     */
    public function getQualifications()
    {
        return $this->qualifications;
    }

    /**
     * Set specialization
     *
     * @param string $specialization
     * @return Cv
     */
    public function setSpecialization($specialization)
    {
        $this->specialization = $specialization;

        return $this;
    }

    /**
     * Get specialization
     *
     * @return string
     */
    public function getSpecialization()
    {
        return $this->specialization;
    }

    /**
     * Set specialization_new
     *
     * @param string $specializationNew
     * @return Cv
     */
    public function setSpecializationNew($specializationNew)
    {
        $this->specialization_new = $specializationNew;

        return $this;
    }

    /**
     * Get specialization_new
     *
     * @return string
     */
    public function getSpecializationNew()
    {
        return $this->specialization_new;
    }

    /**
     * Set institution_name
     *
     * @param string $institutionName
     * @return Cv
     */
    public function setInstitutionName($institutionName)
    {
        $this->institution_name = $institutionName;

        return $this;
    }

    /**
     * Get institution_name
     *
     * @return string
     */
    public function getInstitutionName()
    {
        return $this->institution_name;
    }

    /**
     * Set institution_name_new
     *
     * @param string $institutionNameNew
     * @return Cv
     */
    public function setInstitutionNameNew($institutionNameNew)
    {
        $this->institution_name_new = $institutionNameNew;

        return $this;
    }

    /**
     * Get institution_name_new
     *
     * @return string
     */
    public function getInstitutionNameNew()
    {
        return $this->institution_name_new;
    }

    /**
     * Set start_date_learn
     *
     * @param string $startDateLearn
     * @return Cv
     */
    public function setStartDateLearn($startDateLearn)
    {
        $this->start_date_learn = $startDateLearn;

        return $this;
    }

    /**
     * Get start_date_learn
     *
     * @return string
     */
    public function getStartDateLearn()
    {
        return $this->start_date_learn;
    }

    /**
     * Set start_date_learn_new
     *
     * @param string $startDateLearnNew
     * @return Cv
     */
    public function setStartDateLearnNew($startDateLearnNew)
    {
        $this->start_date_learn_new = $startDateLearnNew;

        return $this;
    }

    /**
     * Get start_date_learn_new
     *
     * @return string
     */
    public function getStartDateLearnNew()
    {
        return $this->start_date_learn_new;
    }

    /**
     * Set end_date_learn
     *
     * @param string $endDateLearn
     * @return Cv
     */
    public function setEndDateLearn($endDateLearn)
    {
        $this->end_date_learn = $endDateLearn;

        return $this;
    }

    /**
     * Get end_date_learn
     *
     * @return string
     */
    public function getEndDateLearn()
    {
        return $this->end_date_learn;
    }

    /**
     * Set end_date_learn_new
     *
     * @param string $endDateLearnNew
     * @return Cv
     */
    public function setEndDateLearnNew($endDateLearnNew)
    {
        $this->end_date_learn_new = $endDateLearnNew;

        return $this;
    }

    /**
     * Get end_date_learn_new
     *
     * @return string
     */
    public function getEndDateLearnNew()
    {
        return $this->end_date_learn_new;
    }

    /**
     * Set other_information_education
     *
     * @param string $otherInformationEducation
     * @return Cv
     */
    public function setOtherInformationEducation($otherInformationEducation)
    {
        $this->other_information_education = $otherInformationEducation;

        return $this;
    }

    /**
     * Get other_information_education
     *
     * @return string
     */
    public function getOtherInformationEducation()
    {
        return $this->other_information_education;
    }

    /**
     * Set other_information_education_new
     *
     * @param string $otherInformationEducationNew
     * @return Cv
     */
    public function setOtherInformationEducationNew($otherInformationEducationNew)
    {
        $this->other_information_education_new = $otherInformationEducationNew;

        return $this;
    }

    /**
     * Get other_information_education_new
     *
     * @return string
     */
    public function getOtherInformationEducationNew()
    {
        return $this->other_information_education_new;
    }

    /**
     * Set skills
     *
     * @param string $skills
     * @return Cv
     */
    public function setSkills($skills)
    {
        $this->skills = $skills;

        return $this;
    }

    /**
     * Get skills
     *
     * @return string
     */
    public function getSkills()
    {
        return $this->skills;
    }

    /**
     * Set competence
     *
     * @param string $competence
     * @return Cv
     */
    public function setCompetence($competence)
    {
        $this->competence = $competence;

        return $this;
    }

    /**
     * Get competence
     *
     * @return string
     */
    public function getCompetence()
    {
        return $this->competence;
    }

    /**
     * Set new_section_1
     *
     * @param string $newSection1
     * @return Cv
     */
    public function setNewSection1($newSection1)
    {
        $this->new_section_1 = $newSection1;

        return $this;
    }

    /**
     * Get new_section_1
     *
     * @return string
     */
    public function getNewSection1()
    {
        return $this->new_section_1;
    }

    /**
     * Set new_section_2
     *
     * @param string $newSection2
     * @return Cv
     */
    public function setNewSection2($newSection2)
    {
        $this->new_section_2 = $newSection2;

        return $this;
    }

    /**
     * Get new_section_2
     *
     * @return string
     */
    public function getNewSection2()
    {
        return $this->new_section_2;
    }

    /**
     * Set new_section_3
     *
     * @param string $newSection3
     * @return Cv
     */
    public function setNewSection3($newSection3)
    {
        $this->new_section_3 = $newSection3;

        return $this;
    }

    /**
     * Get new_section_3
     *
     * @return string
     */
    public function getNewSection3()
    {
        return $this->new_section_3;
    }

    /**
     * Set is_public
     *
     * @param boolean $isPublic
     * @return Cv
     */
    public function setIsPublic($isPublic)
    {
        $this->is_public = $isPublic;

        return $this;
    }

    /**
     * Get is_public
     *
     * @return boolean
     */
    public function getIsPublic()
    {
        return $this->is_public;
    }

    /**
     * Set expires_at
     *
     * @param \DateTime $expiresAt
     * @return Cv
     */
    public function setExpiresAt($expiresAt)
    {
        $this->expires_at = $expiresAt;

        return $this;
    }

    /**
     * Get expires_at
     *
     * @return \DateTime
     */
    public function getExpiresAt()
    {
        return $this->expires_at;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Cv
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return Cv
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set token
     *
     * @param string $token
     * @return Cv
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set language
     *
     * @param string $language
     * @return Cv
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set new_section_1_name
     *
     * @param string $newSection1Name
     * @return Cv
     */
    public function setNewSection1Name($newSection1Name)
    {
        $this->new_section_1_name = $newSection1Name;

        return $this;
    }

    /**
     * Get new_section_1_name
     *
     * @return string
     */
    public function getNewSection1Name()
    {
        return $this->new_section_1_name;
    }

    /**
     * Set new_section_2_name
     *
     * @param string $newSection2Name
     * @return Cv
     */
    public function setNewSection2Name($newSection2Name)
    {
        $this->new_section_2_name = $newSection2Name;

        return $this;
    }

    /**
     * Get new_section_2_name
     *
     * @return string
     */
    public function getNewSection2Name()
    {
        return $this->new_section_2_name;
    }

    /**
     * Set new_section_3_name
     *
     * @param string $newSection3Name
     * @return Cv
     */
    public function setNewSection3Name($newSection3Name)
    {
        $this->new_section_3_name = $newSection3Name;

        return $this;
    }

    /**
     * Get new_section_3_name
     *
     * @return string
     */
    public function getNewSection3Name()
    {
        return $this->new_section_3_name;
    }

    /**
     * Set user
     *
     * @param \Ibw\JobMBundle\Entity\User $user
     * @return Cv
     */
    public function setUser(\Ibw\JobMBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Ibw\JobMBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set city
     *
     * @param \Ibw\JobMBundle\Entity\City $city
     * @return Cv
     */
    public function setCity(\Ibw\JobMBundle\Entity\City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \Ibw\JobMBundle\Entity\City
     */
    public function getCity()
    {
        return $this->city;
    }
    /**
     * @ORM\PrePersist
     */
    public function setTokenValue()
    {
        if(!$this->getToken()) {
            $this->token = sha1($this->getEmail().rand(11111, 99999));
        }
    }

    /**
     * @ORM\PrePersist
     */
    public function preUpload()
    {
        if (null !== $this->file) {
            $this->logo = uniqid() . '.' . $this->file->guessExtension();
        }
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        if(!$this->getCreatedAt()) {
            $this->created_at = new \DateTime();
        }
    }

    /**
     * @ORM\PrePersist
     */
    public function setExpiresAtValue()
    {
        if(!$this->getExpiresAt()) {
            $now = $this->getCreatedAt() ? $this->getCreatedAt()->format('U') : time();
            $this->expires_at = new \DateTime(date('Y-m-d H:i:s', $now + 86400 * 30));
        }
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdatedAtValue()
    {
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PostPersist
     */
    public function upload()
    {
        if (null === $this->file) {
            return;
        }

        // If there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->file->move($this->getUploadRootDir(), $this->logo);

        unset($this->file);
    }

    public function getUploadRootDirImg()
    {
        // absolute path to your directory where images must be saved
        return __DIR__.'/../../../../../web/'.$this->getUploadDir();
    }

    public function getUploadDirImg()
    {
        return 'uploads/myentity';
    }

    public function getAbsolutePathImg()
    {
        return null === $this->image ? null : $this->getUploadRootDir().'/'.$this->image;
    }

    public function getWebPathImg()
    {
        return null === $this->image ? null : '/'.$this->getUploadDir().'/'.$this->image;
    }

    protected function getUploadDir()
    {
        return 'uploads/cv';
    }

    protected function getUploadRootDir()
    {
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    public function getWebPath()
    {
        return null === $this->logo ? null : $this->getUploadDir() . '/' . $this->logo;
    }

    public function getAbsolutePath()
    {
        return null === $this->logo ? null : $this->getUploadRootDir() . '/' . $this->logo;
    }

    /**
     * Set is_activated
     *
     * @param boolean $isActivated
     * @return Cv
     */
    public function setIsActivated($isActivated)
    {
        $this->is_activated = $isActivated;

        return $this;
    }

    /**
     * Get is_activated
     *
     * @return boolean
     */
    public function getIsActivated()
    {
        return $this->is_activated;
    }

    public function publish()
    {
        $this->setIsActivated(true);
    }


    public function extend()
    {
        if (!$this->expiresSoon()) {
            return false;
        }

        $this->expires_at = new \DateTime(date('Y-m-d H:i:s', time() + 86400 * 30));

        return true;
    }


    public function asArray($host)
    {
        return array(
            'company_name' => $this->getCompanyName(),
            'logo' => $this->getLogo() ? 'http://' . $host . '/uploads/cv/' . $this->getLogo() : null,
            'websites' => $this->getWebsites(),
            'city' => $this->getCity(),
            'expires_at' => $this->getCreatedAt()->format('Y-m-d H:i:s'),
        );
    }

    public function isExpired()
    {
        return $this->getDaysBeforeExpires() < 0;
    }

    public function expiresSoon()
    {
        return $this->getDaysBeforeExpires() < 5;
    }

    public function getDaysBeforeExpires()
    {
        return ceil(($this->getExpiresAt()->format('U') - time()) / 86400);
    }
    /**
     * @var \Ibw\JobMBundle\Entity\Category
     */
    private $category;


    /**
     * Set category
     *
     * @param \Ibw\JobMBundle\Entity\Category $category
     * @return Cv
     */
    public function setCategory(\Ibw\JobMBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Ibw\JobMBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }
    /**
     * @var string
     */
    private $nationality;


    /**
     * Set nationality
     *
     * @param string $nationality
     * @return Cv
     */
    public function setNationality($nationality)
    {
        $this->nationality = $nationality;

        return $this;
    }

    /**
     * Get nationality
     *
     * @return string 
     */
    public function getNationality()
    {
        return $this->nationality;
    }
    /**
     * @var \Ibw\JobMBundle\Entity\Candidate
     */
    private $candidate;


    /**
     * Set candidate
     *
     * @param \Ibw\JobMBundle\Entity\Candidate $candidate
     * @return Cv
     */
    public function setCandidate(\Ibw\JobMBundle\Entity\Candidate $candidate = null)
    {
        $this->candidate = $candidate;

        return $this;
    }

    /**
     * Get candidate
     *
     * @return \Ibw\JobMBundle\Entity\Candidate 
     */
    public function getCandidate()
    {
        return $this->candidate;
    }
}
