<?php

namespace Ibw\JobMBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Category
 */
class Category
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $jobs;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $cv;

    /**
     * Constructor
     */
	private $active_jobs;

    /**
     * Constructor
     */
    private $more_jobs;

	
	
	 /**
     * @var string
     */
    private $slug;
	 
    public function __construct()
    {
        $this->jobs = new ArrayCollection();
//        $this->cv = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add jobs
     *
     * @param \Ibw\JobMBundle\Entity\Job $jobs
     * @return Category
     */
    public function addJob(\Ibw\JobMBundle\Entity\Job $jobs)
    {
        $this->jobs[] = $jobs;

        return $this;
    }

    /**
     * Remove jobs
     *
     * @param \Ibw\JobMBundle\Entity\Job $jobs
     */
    public function removeJob(\Ibw\JobMBundle\Entity\Job $jobs)
    {
        $this->jobs->removeElement($jobs);
    }

    /**
     * Get jobs
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getJobs()
    {
        return $this->jobs;
    }

   public function setActiveJobs($jobs)
    {
        $this->active_jobs = $jobs;
    }
 
    public function getActiveJobs()
    {
        return $this->active_jobs;
    }


    public function setMoreJobs($jobs)
    {
        $this->more_jobs = $jobs >=  0 ? $jobs : 0;
    }

    public function getMoreJobs()
    {
        return $this->more_jobs;
    }


    public function __toString()
    {
        return $this->name;
    }


    /**
     * @ORM\PrePersist
     */
    public function setSlugValue()
    {
        $this->slug = Job::slugify($this->getName());
    }


    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }
	
	 /**
     * Set slug
     *
     * @param string $slug
     * @return Category
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }
    /**
     * Add cv
     *
     * @param \Ibw\JobMBundle\Entity\Cv $cv
     * @return Category
     */
    public function addCv(\Ibw\JobMBundle\Entity\Cv $cv)
    {
        $this->cv[] = $cv;

        return $this;
    }

    /**
     * Remove cv
     *
     * @param \Ibw\JobMBundle\Entity\Cv $cv
     */
    public function removeCv(\Ibw\JobMBundle\Entity\Cv $cv)
    {
        $this->cv->removeElement($cv);
    }

    /**
     * Get cv
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCv()
    {
        return $this->cv;
    }
}
