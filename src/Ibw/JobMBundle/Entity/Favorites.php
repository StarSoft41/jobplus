<?php

namespace Ibw\JobMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Favorites
 */
class Favorites
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $job_id;

    /**
     * @var string
     */
    private $cv_id;

    /**
     * @var \Ibw\JobMBundle\Entity\User
     */
    private $user;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set job_id
     *
     * @param string $jobId
     * @return Favorites
     */
    public function setJobId($jobId)
    {
        $this->job_id = $jobId;

        return $this;
    }

    /**
     * Get job_id
     *
     * @return string
     */
    public function getJobId()
    {
        return $this->job_id;
    }

    /**
     * Set cv_id
     *
     * @param string $cvId
     * @return Favorites
     */
    public function setCvId($cvId)
    {
        $this->cv_id = $cvId;

        return $this;
    }

    /**
     * Get cv_id
     *
     * @return string
     */
    public function getCvId()
    {
        return $this->cv_id;
    }


    /**
     * Set user
     *
     * @param \Ibw\JobMBundle\Entity\User $user
     * @return Favorites
     */
    public function setUser(\Ibw\JobMBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Ibw\JobMBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }


}
