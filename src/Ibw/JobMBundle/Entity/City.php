<?php


namespace Ibw\JobMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;




/**
 * City
 */
class City
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name_city;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $jobs;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $cv;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }



    public function setNameCity($name_city)
    {
        $this->name_city = $name_city;

        return $this;
    }

    /**
     * Get name_city
     *
     * @return string
     */
    public function getNameCity()
    {
        return $this->name_city;
    }



    public function __toString()
    {
        return $this->name_city;
    }

    public function __construct()
    {
        $this->jobs = new ArrayCollection();
        $this->cv = new ArrayCollection();
    }


    /**
     * Add jobs
     *
     * @param \Ibw\JobMBundle\Entity\Job $jobs
     * @return City
     */
    public function addJob(\Ibw\JobMBundle\Entity\Job $jobs)
    {
        $this->jobs[] = $jobs;

        return $this;
    }

    /**
     * Remove jobs
     *
     * @param \Ibw\JobMBundle\Entity\Job $jobs
     */
    public function removeJob(\Ibw\JobMBundle\Entity\Job $jobs)
    {
        $this->jobs->removeElement($jobs);
    }

    /**
     * Get jobs
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getJobs()
    {
        return $this->jobs;
    }

    /**
     * Add cv
     *
     * @param \Ibw\JobMBundle\Entity\Cv $cv
     * @return City
     */
    public function addCv(\Ibw\JobMBundle\Entity\Cv $cv)
    {
        $this->cv[] = $cv;

        return $this;
    }

    /**
     * Remove cv
     *
     * @param \Ibw\JobMBundle\Entity\Cv $cv
     */
    public function removeCv(\Ibw\JobMBundle\Entity\Cv $cv)
    {
        $this->cv->removeElement($cv);
    }

    /**
     * Get cv
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCv()
    {
        return $this->cv;
    }
}
