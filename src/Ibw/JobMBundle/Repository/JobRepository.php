<?php

namespace Ibw\JobMBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Ibw\JobMBundle\Entity\Job;

/**
 * JobRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class JobRepository extends EntityRepository
{


    public function getActiveJobs($category_id = null, $max = null, $offset = null)
    {
        $qb = $this->createQueryBuilder('j')
            ->where('j.expires_at > :date')
            ->setParameter('date', date('Y-m-d H:i:s', time()))
            ->orderBy('j.expires_at', 'DESC');

        if($max) {
            $qb->setMaxResults($max);
        }
        if($offset)
        {
            $qb->setFirstResult($offset);
        }

        if($category_id)
        {
            $qb->andWhere('j.category = :category_id')
                ->setParameter('category_id', $category_id);
        }

        $query = $qb->getQuery();

        return $query->getResult();
    }


    public function getForLuceneQuery($query)
    {

        $q = $this->createQueryBuilder('job')
            ->leftJoin('job.city', 'c')
            ->leftJoin('job.company','comp')
            ->where('comp.company_name LIKE :company')
            ->setParameter('company', '%' .$query. '%')
            ->orWhere('c.name_city LIKE :city')
            ->setParameter('city', '%' .$query. '%')
            ->orWhere('job.position_role LIKE  :position')
            ->setParameter('position', '%' .$query. '%')
            ->orWhere('job.payment_job LIKE  :payment')
            ->setParameter('payment', '%' .$query. '%')
            ->setMaxResults(20)
            ->getQuery()->getResult();

        return $q;
    }


    public function countActiveJobs($category_id = null)
    {
        $qb = $this->createQueryBuilder('j')
            ->select('count(j.id)')
            ->where('j.expires_at > :date')
            ->setParameter('date', date('Y-m-d H:i:s', time()));

        if($category_id)
        {
            $qb->andWhere('j.category = :category_id')
                ->setParameter('category_id', $category_id);
        }

        $query = $qb->getQuery();

        return $query->getSingleScalarResult();
    }



    public function getActiveByCategory($category)
    {
        return $this->createQueryBuilder('j')
            ->select('j')
            ->where('j.category = :category')
            ->andWhere('j.expires_at > :date')
            ->setParameters(array(
                'category' => $category,
                'date'     => new \DateTime()
            ))
            ->getQuery()
            ->getResult();
    }



//    public function getActiveByCompany($company)
//    {
//        return $this->createQueryBuilder('j')
//            ->select('j')
//            ->where('j.company = :company')
//            ->andWhere('j.expires_at > :date')
//            ->setParameters(array(
//                'company' => $company,
//                'date'     => new \DateTime()
//            ))
//            ->getQuery()
//            ->getResult();
//    }


    public function getJobs()
    {
        $query = $this->getEntityManager()->createQuery(
            'SELECT j FROM IbwJobMBundle:Job j'
        );

        return $query->getResult();
    }

    public function getPremiumJobs()
    {
        return $this->createQueryBuilder('j')
            ->select('j')
            ->where('j.payment_job =:name')
            ->andWhere('j.expires_at > :date')
            ->andWhere('j.is_activated = :activated')
            ->setParameters(array(
                'date' => new \DateTime(),
                'name' => 'Premium',
                'activated' => 1
            ))
            ->getQuery()
            ->getResult();
    }

    public function getBasicJobs()
    {
        return $this->createQueryBuilder('j')
            ->select('j')
            ->where('j.payment_job = :name')
            ->andWhere('j.expires_at > :date')
            ->andWhere('j.is_activated = :activated')
            ->setParameters(array(
                'date' => new \DateTime(),
                'name' => 'basic',
                'activated' => 1
            ))
            ->getQuery()
            ->getResult();
    }

    public function getStandardJobs()
    {
        return $this->createQueryBuilder('j')
            ->select('j')
            ->where('j.payment_job = :name')
            ->andWhere('j.expires_at > :date')
            ->andWhere('j.is_activated = :activated')
            ->setParameters(array(
                'date' => new \DateTime(),
                'name' => 'standard',
                'activated' => 1
            ))
            ->getQuery()
            ->getResult();
    }

    public  function  getCompanyName()
    {
        return $this->createQueryBuilder('j')
            ->from('IbwJobMBundle:Job', 'j')
            ->select("j")
            ->where('j.payment_job = :name')
            ->andWhere('j.expires_at > :date')
            ->andWhere('j.is_activated = :activated')
            ->andWhere('j.company')
            ->setParameters(array(
                'date' => new \DateTime(),
                'name' => 'premium',
                'activated' => 1
            ))

//            ->leftJoin("Company", "c", "WITH", "j.cid=c.id")
            ->getQuery();
    }

    public function getJobByCompany($idUser)
    {
        return $this->createQueryBuilder('job')
            ->select('job')
            ->where('job.user = :id')
            ->andWhere('job.expires_at > :date')
            ->setParameters(array(
                'id' => $idUser,
                'date'     => new \DateTime()
            ))
            ->getQuery()
            ->getResult();
    }

    public function getJobByUser($id)
    {
        return $this->createQueryBuilder('job')
            ->select('job')
            ->where('job.user = :id')
            ->andWhere('job.expires_at > :date')
            ->setParameters(array(
                'id' => $id,
                'date'     => new \DateTime()
            ))
            ->getQuery()
            ->getResult();
    }

    public function getJobByToken($token)
    {
        return $this->createQueryBuilder('j')
            ->select('j')
            ->where('j.token = :token')
            ->andWhere('j.expires_at > :date')
            ->setParameters(array(
                'token' => $token,
                'date'     => new \DateTime()
            ))
            ->getQuery()
            ->getResult();
    }

    public function getJobToFavorite()
    {
        return  $query = $this->createQueryBuilder('job')
            ->select('job')
//            ->from('foo', 'f')
//            ->where('foo.bar = :id')
//            ->setParameter('id', $myID)
            ->getQuery()
            ->getResult();


//        $total = $query->getSingleScalarResult();
    }

    public function GetFavoritsCompaniesDetails($favoritesComp)
    {
        return $this->createQueryBuilder('j')
            ->select('j')
            ->where('j.id IN (:id)')
//            ->andWhere('j.expires_at > :date')
            ->setParameters(array(
                'id' => $favoritesComp,
//                'date'     => new \DateTime()
            ))
            ->getQuery()
            ->getResult();
    }

}
