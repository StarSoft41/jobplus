<?php

namespace Ibw\JobMBundle\Extension;



class CodeRelicTwigExtension extends \Twig_Extension
{
    private $em;
    private $conn;

    public function __construct(\Doctrine\ORM\EntityManager $em) {
        $this->em = $em;
        $this->conn = $em->getConnection();
    }

    public function getFunctions()
    {
        return array(
            'categories' => new \Twig_Function_Method($this, 'getCategories'),
            'cities'  => new \Twig_Function_Method($this, 'getCities'),
        );
    }

    public function getCategories()
    {
        $sql = "SELECT * FROM category ORDER BY name";
        return $this->conn->fetchAll($sql);
    }

    public function getCities()
    {
        $sql = "SELECT * FROM city";
        return $this->conn->fetchAll($sql);
    }

    public function getName()
    {
        return 'code_relic_twig_extension';
    }
}