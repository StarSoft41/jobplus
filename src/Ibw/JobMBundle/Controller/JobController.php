<?php

namespace Ibw\JobMBundle\Controller;

use Ibw\JobMBundle\Form\CategoryType;
use Ibw\JobMBundle\Form\JobPositionType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Flash;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Ibw\JobMBundle\Repository\JobRepository;
use Ibw\JobMBundle\Entity\Job;
use Ibw\JobMBundle\Entity\User;
use Ibw\JobMBundle\Form\JobType;
use Symfony\Component\HttpFoundation\Response;

/**
 * Job controller.
 *
 */
class JobController extends Controller
{

    /**
     * Lists all Job entities.
     *
     */

    public function indexAction()
    {
        $request = $this->getRequest();

        if ($request->get('_route') == 'IbwJobMBundle_nonlocalized') {
            return $this->redirect($this->generateUrl('ibw_job_m_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        $jobsPremium = $em->getRepository('IbwJobMBundle:Job')->getPremiumJobs();
        $jobsBasic = $em->getRepository('IbwJobMBundle:Job')->getBasicJobs();
        $jobsStandard = $em->getRepository('IbwJobMBundle:Job')->getStandardJobs();
        $comp = $em->getRepository('IbwJobMBundle:Company')->getCompanyName();


        return $this->render('IbwJobMBundle:Job:index.html.twig', array(
            'jobsPremium' => $jobsPremium,
            'jobsBasic' => $jobsBasic,
            'jobsStandard' => $jobsStandard,
            'comp' => $comp,

        ));
    }
    public function showJobByUserAction()
    {
        $id = $this->get('security.context')->getToken()->getUser()->getId();

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('IbwJobMBundle:Job')->getJobByUser($id);

        return $this->render('IbwJobMBundle:Job:show_job_by_user.html.twig', array(
            'user'  => $user,
        ));
    }

    public function publishAction(Request $request, $token)
    {
        $form = $this->createPublishForm($token);
        $form->bind($request);

//        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('IbwJobMBundle:Job')->findOneByToken($token);

            $entity->publish();
            $em->persist($entity);
            $em->flush();

//            $this->get('session')->getFlashBag()->add('notice', 'Your job is now online for 30 days.');
//        }

        return $this->redirect($this->generateUrl('ibw_job_show_by_user'));
    }


    private function createPublishForm($token)
    {
        return $this->createFormBuilder(array('token' => $token))
            ->add('token', 'hidden')
            ->getForm();
    }

    public  function hideAction($token)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('IbwJobMBundle:Job')->findOneByToken($token);

        $entity->setIsActivated('NULL');
        $em->persist($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('ibw_job_show_by_user'));
    }


    /**
     * Creates a new Job entity.
     *
     */
    public function createAction(Request $request)
    {
//        if ($this->get('security.context')->isGranted('IS_AUTHENTICATED_ANONYMOUSLY')) {
//            return $this->redirect($this->generateUrl(
//                'login'
//            ));
//        }
        $entity = new Job();
        $form = $this->createForm(new JobType(), $entity);
        $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $entity->setUser($this->get('security.context')->getToken()->getUser());

                $em->persist($entity);
                $em->flush();

                return $this->redirect($this->generateUrl(
                    'ibw_job_show_by_user'
                ));
            }
        return $this->render('IbwJobMBundle:Job:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

//    /**
//     * Creates a form to create a Job entity.
//     *
//     * @param Job $entity The entity
//     *
//     * @return \Symfony\Component\Form\Form The form
//     */
//    private function createCreateForm(Job $entity)
//    {
//        $form = $this->createForm(new JobType(), $entity, array(
//            'action' => $this->generateUrl('ibw_job_create'),
//            'method' => 'POST',
//        ));
//
//        $form->add('submit', 'submit', array('label' => 'Create'));
//
//        return $form;
//    }

//    public function showJobByCompanyAction()
//    {
//        $id = $this->get('security.context')->getToken()->getUser()->getId();
//
//        $em = $this->getDoctrine()->getManager();
//        $jobCollection = $em->getRepository('IbwJobMBundle:Job')->getJobByCompany($id);
//
//        return $this->render('IbwJobMBundle:Company:show.html.twig', array(
//            'jobCollection'  => $jobCollection,
//        ));
//    }


    /**
     * Finds and displays a Job entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IbwJobMBundle:Job')->find($id);

        return $this->render('IbwJobMBundle:Job:show.html.twig', array(
            'entity' => $entity
        ));
    }

    /**
     * Displays a form to edit an existing Job entity.
     *
     */
    public function editAction($token)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IbwJobMBundle:Job')->findOneByToken($token);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Job entity.');
        }

        $editForm = $this->createForm(new JobType(), $entity);
        $deleteForm = $this->createDeleteForm($token);

        return $this->render('IbwJobMBundle:Job:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Job entity.
     *
     * @param Job $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Job $entity)
    {
        $form = $this->createForm(new JobType(), $entity, array(
            'action' => $this->generateUrl('ibw_job_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing Job entity.
     *
     */
    public function updateAction(Request $request, $token)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IbwJobMBundle:Job')->findOneByToken($token);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Job entity.');
        }

        $editForm = $this->createForm(new JobType(), $entity);
        $deleteForm = $this->createDeleteForm($token);

        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('ibw_job_edit', array('token' => $token)));
        }

        return $this->redirect($this->generateUrl('ibw_job_preview', array(
            'company' => $entity->getCompanySlug(),
            'city' => $entity->getCity(),
            'token' => $entity->getToken(),
            'position' => $entity->getPositionSlug()
        )));
    }

    /**
     * Deletes a Job entity.
     *
     */

    public function deleteAction($token)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('IbwJobMBundle:Job')->findOneByToken($token);

        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('ibw_job_show_by_user'));
    }

    public function CopyJobAction($token)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('IbwJobMBundle:Job')->findOneByToken($token);

        $tokenUpdate = sha1(rand(11111, 99999));
        $JobNameOld = $entity->getPositionRole();
        $JobNameUpdate = 'Copy of '.$JobNameOld;

        $cloneEntity = clone $entity;

        $cloneEntity->setToken($tokenUpdate);
        $cloneEntity->setPositionRole($JobNameUpdate);
        $em->persist($cloneEntity);
        $em->flush();

        return $this->redirect($this->generateUrl('ibw_job_show_by_user'));

    }

    /**
     * Creates a form to delete a Job entity by token.
     *
     * @param mixed $token The entity token
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($token)
    {
        return $this->createFormBuilder(array('token' => $token))
            ->add('token', 'hidden')
            ->getForm();
    }

    public function downloadJobPdfAction($token)
    {

        $em = $this->getDoctrine()->getManager();
        $jobInfo = $em->getRepository('IbwJobMBundle:Job')->getJobByToken($token);
        $entity = $em->getRepository('IbwJobMBundle:Job')->findOneByToken($token);

//        $lang = $entity->getLanguage();
//
//        if($lang == 'en'){
//            $this->get('session')->set('_locale', 'en');
//            $request = $this->getRequest();
//            $request->setLocale('en');
//        }
//        if($lang == 'ru'){
//            $this->get('session')->set('_locale', 'ru');
//            $request = $this->getRequest();
//            $request->setLocale('ru');
//        }
//        if($lang == 'ro'){
//            $this->get('session')->set('_locale', 'ro');
//            $request = $this->getRequest();
//            $request->setLocale('ro');
//        }

        $filename = $entity->getPositionRole();
        $html = $this->renderView('IbwJobMBundle:Job:download_job.html.twig', array(
            'jobInfo' => $jobInfo
        ));

        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
            200,
            array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'attachment; filename=' . $filename
            )
        );

    }

    public function downloadJobDocAction($token)
    {
        $em = $this->getDoctrine()->getManager();
        $jobInfo = $em->getRepository('IbwJobMBundle:Job')->getJobByToken($token);
        $entity = $em->getRepository('IbwJobMBundle:Job')->findOneByToken($token);

//        $lang = $entity->getLanguage();
//
//        if($lang == 'en'){
//            $this->get('session')->set('_locale', 'en');
//            $request = $this->getRequest();
//            $request->setLocale('en');
//        }
//        if($lang == 'ru'){
//            $this->get('session')->set('_locale', 'ru');
//            $request = $this->getRequest();
//            $request->setLocale('ru');
//        }
//        if($lang == 'ro'){
//            $this->get('session')->set('_locale', 'ro');
//            $request = $this->getRequest();
//            $request->setLocale('ro');
//        }

        $filename = $entity->getPositionRole().".doc";

        $response = $this->render('IbwJobMBundle:Job:download_job.html.twig', array(
            'jobInfo' => $jobInfo
        ));
        $response->headers->set('Content-Type', 'application/msword');

        $response->headers->set('Content-Disposition', 'attachment; filename='.$filename);
        return $response;
    }

    public function downloadJobHtmlAction($token){

        $em = $this->getDoctrine()->getManager();
        $jobInfo = $em->getRepository('IbwJobMBundle:Job')->getJobByToken($token);
        $entity = $em->getRepository('IbwJobMBundle:Job')->findOneByToken($token);

//        $htmlTagsWork = $entity->getOtherInformationWork();
//
//        $htmlTags = htmlspecialchars_decode($htmlTagsWork, ENT_NOQUOTES);
//        $entity->setOtherInformationWork($htmlTags);


//        $lang = $entity->getLanguage();
//
//        if($lang == 'en'){
//            $this->get('session')->set('_locale', 'en');
//            $request = $this->getRequest();
//            $request->setLocale('en');
//        }
//        if($lang == 'ru'){
//            $this->get('session')->set('_locale', 'ru');
//            $request = $this->getRequest();
//            $request->setLocale('ru');
//        }
//        if($lang == 'ro'){
//            $this->get('session')->set('_locale', 'ro');
//            $request = $this->getRequest();
//            $request->setLocale('ro');
//        }

        $filename = $entity->getPositionRole().".html";
        $response = $this->render('IbwJobMBundle:Job:download_job.html.twig', array(
            'jobInfo' => $jobInfo
        ));
        $response->headers->set('Content-Type', 'text/html');

        $response->headers->set('Content-Disposition', 'attachment; filename='.$filename);
        return $response;

    }

    public function renameAction($token)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IbwJobMBundle:Job')->findOneByToken($token);
        $renameForm = $this->createForm(new JobPositionType(), $entity);
        return $this->render('IbwJobMBundle:Job:rename_job.html.twig', array(
            'entity' => $entity,
            'rename_form' => $renameForm->createView(),
        ));

    }

    public function updateRenameAction(Request $request, $token)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IbwJobMBundle:Job')->findOneByToken($token);

        $renameForm = $this->createForm(new JobPositionType(), $entity);

        $renameForm->handleRequest($request);

        if ($renameForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('ibw_job_show_by_user'));
        }

        return $this->redirect($this->generateUrl('ibw_job_rename', array(
            'token' => $token,

        )));
    }


    public function previewAction($token)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IbwJobMBundle:Job')->findOneByToken($token);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Job entity.');
        }

        $deleteForm = $this->createDeleteForm($entity->getToken());
        $publishForm = $this->createPublishForm($entity->getToken());

        return $this->render('IbwJobMBundle:Job:show.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
            'publish_form' => $publishForm->createView(),
        ));
    }



    public function searchJobAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $this->getRequest()->get('query');

        if (!$query) {
            if (!$request->isXmlHttpRequest()) {
                return $this->redirect($this->generateUrl('ibw_job'));
            } else {
                return new Response('No results.');
            }
        }

        $jobs = $em->getRepository('IbwJobMBundle:Job')->getForLuceneQuery($query);

        if ($request->isXmlHttpRequest()) {
            if ('*' == $query || !$jobs || $query == '') {
                return new Response('No results.');
            }

            return $this->render('IbwJobMBundle:Job:list_job.html.twig', array(
                'jobs' => $jobs,
            ));
        }

        return $this->render('IbwJobMBundle:Job:search.html.twig', array(
            'jobs' => $jobs,
        ));
    }


    public function AllJobAction()
    {

        $em = $this->getDoctrine()->getManager();
//        $jobs = $em->getRepository('IbwJobMBundle:Job')->getActiveJobs();
//        $companies = $em->getRepository('IbwJobMBundle:Company')->findAll();
        $jobsPremium = $em->getRepository('IbwJobMBundle:Job')->getPremiumJobs();
        $jobsBasic = $em->getRepository('IbwJobMBundle:Job')->getBasicJobs();
        $jobsStandard = $em->getRepository('IbwJobMBundle:Job')->getStandardJobs();
        $comp = $em->getRepository('IbwJobMBundle:Company')->getCompanyName();

        return $this->render('IbwJobMBundle:Job:all_jobs.html.twig', array(
            'jobsPremium' => $jobsPremium,
            'jobsBasic' => $jobsBasic,
            'jobsStandard' => $jobsStandard,
            'comp' => $comp,
        ));


    }


    public function JobCatAction($categoryId)
    {

        $em = $this->getDoctrine()->getManager();

        $jobCollection = $em->getRepository('IbwJobMBundle:Job')->getActiveByCategory($categoryId);

        return $this->render('IbwJobMBundle:Job:job_for_category.html.twig', array(
            'jobCollection' => $jobCollection,
            'count' => count($jobCollection)
        ));
    }


    public function AvdancedSearchAction()
    {

        $em = $this->getDoctrine()->getManager();

        $jobCollection = $em->getRepository('IbwJobMBundle:Job')->getJobs();
        $companyCollection = $em->getRepository('IbwJobMBundle:Company')->findAll();
        $categoryCollection = $em->getRepository('IbwJobMBundle:Category')->findAll();
        $cityCollection = $em->getRepository('IbwJobMBundle:City')->findAll();
//        $typeCollection = $em->getRepository('IbwJobMBundle:Job')->findAll();
        return $this->render('IbwJobMBundle:Job:advanced_search.html.twig', array(
            'jobCollection' => $jobCollection,
            'companyCollection' => $companyCollection,
            'categoryCollection' => $categoryCollection,
            'cityCollection' => $cityCollection,
//            'typeCollection' => $typeCollection
        ));
    }


//    /**
//     * @Route("/remindPassword", name="_remind_password")
//     */
//    public function remindPasswordAction()
//    {
//        $req = $this->getRequest()->request->all();
//        $username = $req['user_name'];
//
//        $errorMessage = $this->get('translator')->trans('remindPassword.customer_error', array('%s%' => $username));
//
//        if (!isset($username) || $username == "") {
//
//            return new Response(json_encode(array('nr' => 1, 'message' => $errorMessage)));
//        }
//
//        /** @var \Feedify\BaseBundle\Service\Management\Customer $legacyCustomer */
//        $legacyCustomer = $this->get('feedify.legacy_customer');
//        try {
//            $customer = $legacyCustomer->getCustomer($username);
//        } catch (\Exception $e) {
//
//            return new Response(json_encode(array('nr' => 1, 'message' => $errorMessage)), 200);
//        }
//
//        /** @var \Symfony\Component\Security\Core\Encoder\EncoderFactory $encoderFactory */
//        $encoderFactory = $this->get('security.encoder_factory');
//        $encoder = $encoderFactory->getEncoder($customer);
//
//        $salt = md5($legacyCustomer->generateRandomString() . time());
//        $customer->setSalt($salt);
//        $randStr = $legacyCustomer->generateRandomString();
//        $password = $encoder->encodePassword($randStr, $customer->getSalt());
//        $customer->setPassword($password);
//
//        $legacyCustomer->setCustomer($customer);
//        $ok = $legacyCustomer->saveCustomer();
//
//        if ($ok) {
//            $message = \Swift_Message::newInstance()
//                ->setSubject('Feedify information')
//                ->setFrom('info@feedify.de')
//                ->setReplyTo('no-replay@feedify.de')
//                ->setTo($customer->getEmail())
//                ->setContentType("text/html")
//                ->setBody(
//                    $this->renderView(
//                        'FeedifyInterfaceBundle:Mails:password_reminder.html.twig',
//                        array(
//                            'name' => $customer->getFirstName(),
//                            'lastname' => $customer->getLastName(),
//                            'password' => $randStr
//                        )
//                    )
//                );
//
//
//            if ($this->get('mailer')->send($message)) {
//                $errorMessage = $this->get('translator')->trans('remindPassword.mail_success');
//                $content = json_encode(array('nr' => 0, 'message' => $errorMessage));
//            } else {
//                $errorMessage = $this->get('translator')->trans('remindPassword.mail_failed');
//                $content = json_encode(array('nr' => 1, 'message' => $errorMessage));
//            }
//
//        } else {
//            $errorMessage = $this->get('translator')->trans('remindPassword.mail_failed');
//            $content = json_encode(array('nr' => 1, 'message' => $errorMessage));
//        }
//
//        return new Response($content, 200);
//    }

}
