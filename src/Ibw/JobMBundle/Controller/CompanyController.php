<?php

namespace Ibw\JobMBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Ibw\JobMBundle\Repository\CompanyRepository;
use Ibw\JobMBundle\Entity\Company;
use Ibw\JobMBundle\Form\CompanyType;

class CompanyController extends Controller
{

    public function createCompanyAction(Request $request)
    {
        $entity  = new Company();
        $form = $this->createForm(new CompanyType(),$entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
//            $entity->setUser($this->get('security.context')->getToken()->getUser());
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl(
                'ibw_company_show'
            ));
        }
        return $this->render('IbwJobMBundle:Company:company_new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }




    public function showCompanyAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $idUser = $this->get('security.context')->getToken()->getUser()->getId();
        $entities = $em->getRepository('IbwJobMBundle:Company')->find($id);
        $jobCollection = $em->getRepository('IbwJobMBundle:Job')->getJobByCompany($idUser);

        return $this->render('IbwJobMBundle:Company:show.html.twig', array(
            'entities' => $entities,
            'jobCollection' => $jobCollection,
//            'count' => count($jobCollection)
        ));
    }

}
