<?php

namespace Ibw\JobMBundle\Controller;

use Ibw\JobMBundle\Entity\Favorites;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class FavoritesController extends Controller
{

    public function showFavoritesUsersAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $id = $this->get('security.context')->getToken()->getUser()->getId();

        $favoritesUsers = $em->getRepository('IbwJobMBundle:Favorites')->getFavoritUser($id);
        $favoritesUserDetails = $em->getRepository('IbwJobMBundle:Cv')->GetFavoritsUserDetails($favoritesUsers);

        $total = count($favoritesUsers);

        return $this->render('IbwJobMBundle:Favorites:favorites_users.html.twig', array(
            'favoritesUsersDetails' => $favoritesUserDetails,
            'total'  => $total

        ));
    }

    public function showFavoritesCompaniesAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $id = $this->get('security.context')->getToken()->getUser()->getId();

        $favoritesComp = $em->getRepository('IbwJobMBundle:Favorites')->GetFavoritsCompanies($id);
        $favoritesCompDetails = $em->getRepository('IbwJobMBundle:Job')->GetFavoritsCompaniesDetails($favoritesComp);

        $total = count($favoritesComp);

        return $this->render('IbwJobMBundle:Favorites:favorites_companies.html.twig', array(
            'total'  => $total,
            'favoritesCompDetails' => $favoritesCompDetails,

        ));

    }

    public function addFavoritesUserAction(Request $request, $token)
    {
        $form = $this->createAddForm($token);
        $form->bind($request);

        $FavoritesUser = new Favorites();
        $em = $this->getDoctrine()->getManager();
        $idUser = $this->get('security.context')->getToken()->getUser()->getId();

        $entity = $em->getRepository('IbwJobMBundle:Cv')->findOneByToken($token)->getId();

        $a = $em->getRepository('IbwJobMBundle:Favorites')->FindUserIfExist($entity, $idUser);
        if( empty( $a ) )
        {
            $FavoritesUser->setJobId($entity);
            $FavoritesUser->setUser($this->get('security.context')->getToken()->getUser())->getId();

            $em->persist($FavoritesUser);
            $em->flush();
        }
        foreach ($a as $inner) {
            if(isset($inner['cv_id']) && $inner['cv_id'] == $entity) {

                return $this->render('IbwJobMBundle:Account:access_denied.html.twig');

            }
            else{
                $FavoritesUser->setJobId($entity);
                $FavoritesUser->setUser($this->get('security.context')->getToken()->getUser())->getId();

                $em->persist($FavoritesUser);
                $em->flush();

            }
        }
        $this->get('session')->getFlashBag()->add('message', 'User Added to Favorite');
        $referer = $this->getRequest()->headers->get('referer');
        return $this->redirect($referer);
    }

    public function addFavoritesCompanyAction(Request $request, $token)
    {
        $form = $this->createAddForm($token);
        $form->bind($request);

        $FavoritesCompany = new Favorites();
        $em = $this->getDoctrine()->getManager();
        $idUser = $this->get('security.context')->getToken()->getUser()->getId();

        $entity = $em->getRepository('IbwJobMBundle:Job')->findOneByToken($token)->getId();

        $a = $em->getRepository('IbwJobMBundle:Favorites')->FindJobIfExist($entity, $idUser);
        if( empty( $a ) )
        {
            $FavoritesCompany->setJobId($entity);
            $FavoritesCompany->setUser($this->get('security.context')->getToken()->getUser())->getId();

            $em->persist($FavoritesCompany);
            $em->flush();
        }
        foreach ($a as $inner) {
            if(isset($inner['job_id']) && $inner['job_id'] == $entity) {

                return $this->render('IbwJobMBundle:Account:access_denied.html.twig');

            }
            else{
                $FavoritesCompany->setJobId($entity);
                $FavoritesCompany->setUser($this->get('security.context')->getToken()->getUser())->getId();

                $em->persist($FavoritesCompany);
                $em->flush();

            }
        }

        $this->get('session')->getFlashBag()->add('message', 'Job Added to Favorite');
        $referer = $this->getRequest()->headers->get('referer');
        return $this->redirect($referer);

    }

    private function createAddForm($token)
    {
        return $this->createFormBuilder(array('token' => $token))
            ->add('token', 'hidden')
            ->getForm();
    }

    public function deleteFavoritesCompanyAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('IbwJobMBundle:Favorites')->DeleteJobById($id);
        return $this->redirect($this->generateUrl('my_favorits_companies'));
    }

    public function deleteFavoritesUserAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('IbwJobMBundle:Favorites')->DeleteUserById($id);
        return $this->redirect($this->generateUrl('my_favorits_users'));
    }

}
