<?php

namespace Ibw\JobMBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{

    public function changeLanguageAction()
    {
        $language = $this->getRequest()->get('language');
        return $this->redirect($this->generateUrl('ibw_job_m_homepage', array('_locale' => $language)));
    }


//    public function showCompanyAction($company, $id)
//    {
//        $em = $this->getDoctrine()->getManager();
//
////        $entity = $em->getRepository('IbwJobMBundle:Job')->find($id);
//        $entities = $em->getRepository('IbwJobMBundle:Company')->find($id);
//        $jobCollection = $em->getRepository('IbwJobMBundle:Job')->getActiveByCompany($company);
//
//        return $this->render('IbwJobMBundle:Company:show.html.twig', array(
//            'entities' => $entities,
////            'entity' => $entity,
//            'jobCollection' => $jobCollection,
//            'count' => count($jobCollection)
//        ));
//    }


    public function searchAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $keyword = $this->getRequest()->get('query');
        $categories = $this->getRequest()->get('category');
        $cities = $this->getRequest()->get('location');
        $industries = $this->getRequest()->get('industry');

       // echo $keyword . "<br/>";

//        if ($categories) {
//            foreach ($categories as $category) {
//               echo "<span style='color:red'>" . $category . "</span><br/>";
//            }
//            echo "<br/>";
//        }
//        if ($cities) {
//            foreach ($cities as $city) {
//                echo "<span style='color:#808080'>" . $city . "</span><br/>";
//            }
//
//            echo "<br/>";
//        }
//        if ($industries) {
//            foreach ($industries as $industry) {
//                echo "<span style='color:green'>" . $industry . "</span><br/>";
//            }
//        }
        //die;

//        if (!$query) {
//            if (!$request->isXmlHttpRequest()) {
//                return $this->redirect($this->generateUrl('ibw_job_m_homepage'));
//            } else {
//                return new Response('No results.');
//            }
//        }

//        $jobs = $em->getRepository('IbwJobMBundle:Job')->getForLuceneQuery($keyword);
        $resume = $em->getRepository('IbwJobMBundle:Cv')->getForLuceneQuery($keyword);

//        if ($jobs == false && $cv == false) {
//            return new Response('for this  '.$keyword .'  No results.');
//            //return $this->render('IbwJobMBundle:Search:no_result.html.twig');
//
//        } else {

            return $this->render('IbwJobMBundle:Cv:search_cv.html.twig', array(
//                'jobs' => $jobs,
                'resume' => $resume
            ));
//        }
    }


    public function ConditionsAction()
    {
        return $this->render('IbwJobMBundle:Default:conditions.html.twig');
    }

//    public function MapsAction($address)
//    {
//        $em = $this->getDoctrine()->getManager();
//
//        $entity = $em->getRepository('IbwJobMBundle:Company')->findAll($address);
//
//        $address = str_replace(" ", "+", $address); // replace all the white space with "+" sign to match with google search pattern
//
//        $url = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=$address";
//
//        $response = file_get_contents($url);
//
//        $json = json_decode($response,TRUE); //generate array object from the response from the web
//        print_r($json['results'][0]['geometry']['location']['lat'].","
//            .$json['results'][0]['geometry']['location']['lng']);die;
//       $m = $json['results'][0]['geometry']['location']['lat'].",".$json['results'][0]['geometry']['location']['lng'];
//
//
//        return $this->render('IbwJobMBundle:Default:maps.html.twig',array(
////            'json' =>( $json['results'][0]['geometry']['location']['lat'].","
////            .$json['results'][0]['geometry']['location']['lng']),
//            'entity' => $entity,
//            'm' => $m
//        ));
//
//    }

    public function MapsAction()
    {

        $em = $this->getDoctrine()->getManager();

        $address = $em->getRepository('IbwJobMBundle:Company')->getStreet();
        $description = $em->getRepository('IbwJobMBundle:Company')->getDescription();

        foreach ($address as $add) {
            $url = "http://maps.google.com/maps/api/geocode/json?address=".urlencode($add['company_country'].','.$add['company_city'].','.$add['company_street']. "&sensor=false") ;

            $response = file_get_contents($url);

            $json = json_decode($response,TRUE);

//
            $m[] = $response;
            $m = $json['results'][0]['geometry']['location']['lat'].",".$json['results'][0]['geometry']['location']['lng'];
        }
        return $this->render('IbwJobMBundle:Default:maps.html.twig',array(
            'm' => $m,
            'description' => $description
        ));

    }




}
