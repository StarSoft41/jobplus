<?php

namespace Ibw\JobMBundle\Controller;

use Ibw\JobMBundle\Entity\Cv;
use Ibw\JobMBundle\Form\CvType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder;
use Symfony\Component\HttpFoundation\Response;
use Ibw\JobMBundle\Entity\User;
use Ibw\JobMBundle\Form\CvNameType;

class CvController extends Controller
{

    public function createCvAction(Request $request)
    {
        $entity  = new Cv();

        $lang = $request->get('lang', $request->getLocale());
        if($lang == 'en'){
            $this->get('session')->set('_locale', 'en');
            $request = $this->getRequest();
            $request->setLocale('en');
        }
        if($lang == 'ru'){
            $this->get('session')->set('_locale', 'ru');
            $request = $this->getRequest();
            $request->setLocale('ru');
        }
        if($lang == 'ro'){
            $this->get('session')->set('_locale', 'ro');
            $request = $this->getRequest();
            $request->setLocale('ro');
        }


        $entity->setCvName($request->get('title'));
        $entity->setLanguage($lang);
        $form = $this->createForm(new CvType(),$entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setUser($this->get('security.context')->getToken()->getUser());
//            $regexWork = strip_tags('<ul><li>row 1</li><li>row 2</li><li>row 3</li></ul>');
//            $regexEducation = strip_tags('<ul><li>row 1</li><li>row 2</li><li>row 3</li></ul>');
//            $entity->setOtherInformationWork($regexWork);
//            $entity->setOtherInformationEducation($regexEducation);

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl(
                'ibw_cv_show_by_user'
            ));
        }
        return $this->render('IbwJobMBundle:Cv:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    public function showCvByUserAction()
    {
        $id = $this->get('security.context')->getToken()->getUser()->getId();

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('IbwJobMBundle:Cv')->getCvByUser($id);

        return $this->render('IbwJobMBundle:Cv:show_cv_by_user.html.twig', array(
            'user'  => $user,
        ));
    }

    public function downloadCvPdfAction($token)
    {

        $em = $this->getDoctrine()->getManager();
        $cvInfo = $em->getRepository('IbwJobMBundle:Cv')->getCvByToken($token);
        $entity = $em->getRepository('IbwJobMBundle:Cv')->findOneByToken($token);

        $lang = $entity->getLanguage();

        if($lang == 'en'){
            $this->get('session')->set('_locale', 'en');
            $request = $this->getRequest();
            $request->setLocale('en');
        }
        if($lang == 'ru'){
            $this->get('session')->set('_locale', 'ru');
            $request = $this->getRequest();
            $request->setLocale('ru');
        }
        if($lang == 'ro'){
            $this->get('session')->set('_locale', 'ro');
            $request = $this->getRequest();
            $request->setLocale('ro');
        }

        $filename = $entity->getCvName();
        $html = $this->renderView('IbwJobMBundle:Cv:download_cv.html.twig', array(
            'cvInfo' => $cvInfo
        ));

        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
            200,
            array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'attachment; filename=' . $filename
            )
        );

    }

    public function downloadCvDocAction($token)
    {
        $em = $this->getDoctrine()->getManager();
        $cvInfo = $em->getRepository('IbwJobMBundle:Cv')->getCvByToken($token);
        $entity = $em->getRepository('IbwJobMBundle:Cv')->findOneByToken($token);

        $lang = $entity->getLanguage();

        if($lang == 'en'){
            $this->get('session')->set('_locale', 'en');
            $request = $this->getRequest();
            $request->setLocale('en');
        }
        if($lang == 'ru'){
            $this->get('session')->set('_locale', 'ru');
            $request = $this->getRequest();
            $request->setLocale('ru');
        }
        if($lang == 'ro'){
            $this->get('session')->set('_locale', 'ro');
            $request = $this->getRequest();
            $request->setLocale('ro');
        }

        $filename = $entity->getCvName().".doc";

        $response = $this->render('IbwJobMBundle:Cv:download_cv.html.twig', array(
            'cvInfo' => $cvInfo
        ));
        $response->headers->set('Content-Type', 'application/msword');

        $response->headers->set('Content-Disposition', 'attachment; filename='.$filename);
        return $response;
    }

    public function downloadCvHtmlAction($token){

        $em = $this->getDoctrine()->getManager();
        $cvInfo = $em->getRepository('IbwJobMBundle:Cv')->getCvByToken($token);
        $entity = $em->getRepository('IbwJobMBundle:Cv')->findOneByToken($token);

//        $htmlTagsWork = $entity->getOtherInformationWork();
//
//        $htmlTags = htmlspecialchars_decode($htmlTagsWork, ENT_NOQUOTES);
//        $entity->setOtherInformationWork($htmlTags);


        $lang = $entity->getLanguage();

        if($lang == 'en'){
            $this->get('session')->set('_locale', 'en');
            $request = $this->getRequest();
            $request->setLocale('en');
        }
        if($lang == 'ru'){
            $this->get('session')->set('_locale', 'ru');
            $request = $this->getRequest();
            $request->setLocale('ru');
        }
        if($lang == 'ro'){
            $this->get('session')->set('_locale', 'ro');
            $request = $this->getRequest();
            $request->setLocale('ro');
        }

        $filename = $entity->getCvName().".html";
        $response = $this->render('IbwJobMBundle:Cv:download_cv.html.twig', array(
            'cvInfo' => $cvInfo
        ));
        $response->headers->set('Content-Type', 'text/html');

        $response->headers->set('Content-Disposition', 'attachment; filename='.$filename);
        return $response;

    }

    /**
     * Displays a form to edit an existing Cv entity.
     *
     */
    public function editAction($token)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('IbwJobMBundle:Cv')->findByToken($token);
        $entity = $em->getRepository('IbwJobMBundle:Cv')->findOneByToken($token);
        $lang = $entity->getLanguage();
        if($lang == 'en'){
            $this->get('session')->set('_locale', 'en');
            $request = $this->getRequest();
            $request->setLocale('en');
        }
        if($lang == 'ru'){
            $this->get('session')->set('_locale', 'ru');
            $request = $this->getRequest();
            $request->setLocale('ru');
        }
        if($lang == 'ro'){
            $this->get('session')->set('_locale', 'ro');
            $request = $this->getRequest();
            $request->setLocale('ro');
        }

        $editForm = $this->createForm(new CvType(), $entity);
        $deleteForm = $this->createDeleteForm($token);

        return $this->render('IbwJobMBundle:Cv:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'user'  => $user
        ));

    }

    public function renameAction($token)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IbwJobMBundle:Cv')->findOneByToken($token);
        $renameForm = $this->createForm(new CvNameType(), $entity);
        return $this->render('IbwJobMBundle:Cv:rename.html.twig', array(
            'entity' => $entity,
            'rename_form' => $renameForm->createView(),
        ));

    }

    public function previewCvAction($token)
    {
        $em = $this->getDoctrine()->getManager();

        $resume = $em->getRepository('IbwJobMBundle:Cv')->getCvByToken($token);

        return $this->render('IbwJobMBundle:Cv:preview_after_sharing.html.twig', array(
            'resume' => $resume
        ));
    }

    private function createDeleteForm($token)
    {
        return $this->createFormBuilder(array
        ('token' => $token

        ))
            ->add('token', 'hidden')
            ->getForm();
    }


    /**
     * Edits an existing Cv entity.
     *
     */
    public function updateAction(Request $request, $token)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IbwJobMBundle:Cv')->findOneByToken($token);

        $editForm = $this->createForm(new CvType(), $entity);

        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
//            $regexWork = strip_tags('<ul><li>row 1</li><li>row 2</li><li>row 3</li></ul>');
//            $regexEducation = strip_tags('<ul><li>row 1</li><li>row 2</li><li>row 3</li></ul>');
//            $entity->setOtherInformationWork($regexWork);
//            $entity->setOtherInformationEducation($regexEducation);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('ibw_cv_show_by_user'));
        }

        return $this->redirect($this->generateUrl('ibw_cv_edit', array(
            'token' => $token
        )));

    }

    public function updateRenameAction(Request $request, $token)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IbwJobMBundle:Cv')->findOneByToken($token);

        $renameForm = $this->createForm(new CvNameType(), $entity);

        $renameForm->handleRequest($request);

        if ($renameForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('ibw_cv_show_by_user'));
        }

        return $this->redirect($this->generateUrl('ibw_cv_rename', array(
            'token' => $token,

        )));
    }

    public function deleteAction($token)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('IbwJobMBundle:Cv')->findOneByToken($token);

        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('ibw_cv_show_by_user'));
    }

    public function CopyCvAction($token)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('IbwJobMBundle:Cv')->findOneByToken($token);

        $tokenUpdate = sha1(rand(11111, 99999));
        $CvNameOld = $entity->getCvName();
        $CvNameUpdate = 'Copy of '.$CvNameOld;

        $cloneEntity = clone $entity;

        $cloneEntity->setToken($tokenUpdate);
        $cloneEntity->setCvName($CvNameUpdate);
        $em->persist($cloneEntity);
        $em->flush();

        return $this->redirect($this->generateUrl('ibw_cv_show_by_user'));

    }

    public function searchCvAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $keyword = $this->getRequest()->get('query');

        $resume = $em->getRepository('IbwJobMBundle:Cv')->getForLuceneQuery($keyword);

        return $this->render('IbwJobMBundle:Cv:search_cv.html.twig', array(
            'resume' => $resume
        ));
    }

//    public function createCvAction(Request $request)
//    {
//        $entity  = new Cv();
//        $form = $this->createForm(new CvType(),$entity);
//        $form->handleRequest($request);
//
//        if ($form->isValid()) {
//            $em = $this->getDoctrine()->getManager();
//
//            $em->persist($entity);
//            $em->flush();
//
//            return $this->redirect($this->generateUrl('ibw_cv_show', array(
////                'category' => $entity->getCategory(),
//                'city' => $entity->getCity(),
//                'id' => $entity->getId(),
//                'name' => $entity->getName()
//
//            )));
//        }
//
//
//        return $this->render('IbwJobMBundle:Cv:new.html.twig', array(
//            'entity' => $entity,
//            'form'   => $form->createView(),
//        ));
//
//    }
//
//
//    public function newCvAction()
//    {
//       // $id = $this->get('security.context')->getToken()->getUser()->getId();
//
//        if ($this->get('security.context')->isGranted('ROLE_USER') == false) {
//            //throw new AccessDeniedException();
//            return $this->render('IbwJobMBundle:Account:access_denied.html.twig');
//        }
//        $em = $this->getDoctrine()->getManager();
//      //  $user = $em->getRepository('IbwJobMBundle:Cv')->getCvByUser($id);
//        $entity = new Cv();
//
//        $form = $this->createForm(new CvType(), $entity);
//
//        return $this->render('IbwJobMBundle:Cv:new.html.twig', array(
//            'entity' => $entity,
//            'form'   => $form->createView(),
//        //    'user'  => $user
//
//
//        ));
//    }
//
//    public function showCvByUserAction()
//    {
//        $id = $this->get('security.context')->getToken()->getUser()->getId();
//
//        if ($this->get('security.context')->isGranted('ROLE_USER') == false) {
//            //throw new AccessDeniedException();
//            return $this->render('IbwJobMBundle:Account:access_denied.html.twig');
//        }
//        $em = $this->getDoctrine()->getManager();
//        $user = $em->getRepository('IbwJobMBundle:Cv')->getCvByUser($id);
//
//        return $this->render('IbwJobMBundle:Cv:show_cv_by_user.html.twig', array(
//            'user'  => $user
//        ));
//    }
//
//
//
//
//
//    public function previewCvAction($name)
//    {
//        $em = $this->getDoctrine()->getManager();
//
//        $entity = $em->getRepository('IbwJobMBundle:Cv')->find($name);
//
//        if (!$entity) {
//            throw $this->createNotFoundException('Unable to find Cv entity.');
//        }
//
//        return $this->render('IbwJobMBundle:Cv:show.html.twig', array(
//            'entity'      => $entity,
//        ));
//    }
//
//
//    /**
//     * Displays a form to edit an existing Cv entity.
//     *
//     */
//    public function editAction($token)
//    {
//        $em = $this->getDoctrine()->getManager();
//
//        $entity = $em->getRepository('IbwJobMBundle:Cv')->findOneByToken($token);
//
////        if (!$entity) {
////            throw $this->createNotFoundException('Unable to find Cv entity.');
////        }
//
//        $editForm = $this->createForm(new CvType(), $entity);
//        $deleteForm = $this->createDeleteForm($token);
//
//        return $this->render('IbwJobMBundle:Cv:edit.html.twig', array(
//            'entity' => $entity,
//            'edit_form' => $editForm->createView(),
//            'delete_form' => $deleteForm->createView(),
//        ));
//    }
//
//    private function createDeleteForm($token)
//    {
//        return $this->createFormBuilder(array('token' => $token))
//            ->add('token', 'hidden')
//            ->getForm();
//    }
//
//
//    /**
//     * Edits an existing Cv entity.
//     *
//     */
//    public function updateAction(Request $request, $token)
//    {
//        $em = $this->getDoctrine()->getManager();
//
//        $entity = $em->getRepository('IbwJobMBundle:Cv')->findOneByToken($token);
//
//        if (!$entity) {
//            throw $this->createNotFoundException('Unable to find Cv entity.');
//        }
//
//        $editForm = $this->createForm(new CvType(), $entity);
//       // $deleteForm = $this->createDeleteForm($token);
//
//        $editForm->bind($request);
//
//        if ($editForm->isValid()) {
//            $em->persist($entity);
//            $em->flush();
//
//            return $this->redirect($this->generateUrl('ibw_cv_edit', array('token' => $token)));
//        }
//
//        return $this->redirect($this->generateUrl('ibw_cv_preview', array(
//            'token' => $entity->getToken(),
//
//        )));
//    }
//
//
//    public function previewAction($token)
//    {
//        $em = $this->getDoctrine()->getManager();
//
//        $entity = $em->getRepository('IbwJobMBundle:Cv')->findOneByToken($token);
//
//        if (!$entity) {
//            throw $this->createNotFoundException('Unable to find Cv entity.');
//        }
//
//        $deleteForm = $this->createDeleteForm($entity->getToken());
//        $publishForm = $this->createPublishForm($entity->getToken());
//
//        return $this->render('IbwJobMBundle:Cv:show.html.twig', array(
//            'entity' => $entity,
//            'delete_form' => $deleteForm->createView(),
//            'publish_form' => $publishForm->createView(),
//        ));
//    }
//
//    private function createPublishForm($token)
//    {
//        return $this->createFormBuilder(array('token' => $token))
//            ->add('token', 'hidden')
//            ->getForm();
//    }
//
//
//    public function showCvAction($id)
//    {
//        $em = $this->getDoctrine()->getManager();
//
//        $entity = $em->getRepository('IbwJobMBundle:Cv')->find($id);
//
//        if (!$entity) {
//            throw $this->createNotFoundException('Unable to find Cv entity.');
//        }
//
//        $cv = array('id' => $entity->getId(), 'city' => $entity->getCity(), 'name' => $entity->getName());
//
//        if (!$cv) {
//
//            delete($cv);
//        }
//
//        return $this->render('IbwJobMBundle:Cv:show.html.twig', array(
//            'entity'      => $entity,
//
//        ));
//    }
//
//
//    public function CvCatAction($categoryId)
//    {
//
//        $em = $this->getDoctrine()->getManager();
//
//        $cvCollection = $em->getRepository('IbwJobMBundle:Cv')->getActiveByCategory($categoryId);
//
//        return $this->render('IbwJobMBundle:Cv:all_cv.html.twig', array(
//            'cvCollection'    => $cvCollection,
//            'count' => count($cvCollection)
//        ));
//    }
//
//
//
//    public function searchCvAction(Request $request)
//    {
//        $em = $this->getDoctrine()->getManager();
//        $keyword = $this->getRequest()->get('query');
//        //$this->get('request_stack')->getCurrentRequest()
//
//        if(!$keyword) {
//            if(!$request->isXmlHttpRequest()) {
//                return $this->redirect($this->generateUrl('ibw_cv'));
//            } else {
//                return new Response('No results.');
//            }
//        }
//
//        $resume = $em->getRepository('IbwJobMBundle:Cv')->getForLuceneQuery($keyword);
//
//        if($request->isXmlHttpRequest()) {
//            if('*' == $keyword || !$resume || $keyword == '') {
//                return new Response('No results.');
//            }
//
//            return $this->render('IbwJobMBundle:Cv:index_cv.html.twig', array(
//                'resume' => $resume
//            ));
//
//        }
//
//        return $this->render('IbwJobMBundle:Cv:search_cv.html.twig', array(
//            'resume' => $resume
//        ));
//    }
//
//
//    public  function AvdancedSearchAction()
//    {
//
//        $em = $this->getDoctrine()->getManager();
//
//        $cvCollection = $em->getRepository('IbwJobMBundle:Cv')->getCv();
//        $cityCollection = $em->getRepository('IbwJobMBundle:City')->findAll();
//        $categoryCollection = $em->getRepository('IbwJobMBundle:Category')->findAll();
//        return $this->render('IbwJobMBundle:Cv:advanced_search.html.twig', array(
//            'cvCollection' => $cvCollection,
//            'cityCollection' => $cityCollection,
//            'categoryCollection' => $categoryCollection
//        ));
//    }
//
//
//    public function AllCVAction()
//    {
//
//        $em = $this->getDoctrine()->getManager();
//        $cvCollection = $em->getRepository('IbwJobMBundle:Cv')->getCv();
//
//        return $this->render('IbwJobMBundle:Cv:all_cv.html.twig', array(
//            'cvCollection' => $cvCollection,
//
//        ));
//
//
//    }



}