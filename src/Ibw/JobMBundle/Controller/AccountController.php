<?php

namespace Ibw\JobMBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Ibw\JobMBundle\Entity\Candidate;
use Ibw\JobMBundle\Entity\Role;
use Ibw\JobMBundle\Entity\User;
use Ibw\JobMBundle\Form\CandidateType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Ibw\JobMBundle\Form\RegistrationType;
use Symfony\Component\Security\Core\Encoder;
use Symfony\Component\Security\Core\SecurityContext;

class AccountController extends Controller
{



    public function loginAction()
    {
        $request = $this->getRequest();
        $session = $request->getSession();

        // get the login error if there is one
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }

        return $this->render('IbwJobMBundle:Account:login.html.twig', array(
            'last_username' => $session->get(SecurityContext::LAST_USERNAME),
            'error' => $error,
        ));
    }

    public function registerAction()
    {

        $userEntity = new User();

        $form = $this->createForm(new RegistrationType(), $userEntity, array(
            'action' => $this->generateUrl('account_create'),
        ));

        return $this->render(
            'IbwJobMBundle:Account:register.html.twig',
            array('form' => $form->createView())
        );
    }

    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new RegistrationType(), new User());
        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var User $user */
            $user = $form->getData();
            $role = new Role();

            if ($user->getSelect() == 'company') {
                $role->setUser($user)
                    ->setRole('ROLE_COMPANY');

                $user->addRole($role);
            } elseif ($user->getSelect() == 'candidate') {
                $role->setUser($user)
                    ->setRole('ROLE_USER');

                $user->addRole($role);
            } else {
                return $this->render(
                    'IbwJobMBundle:Account:register.html.twig',
                    array('form' => $form->createView())
                );
            }

            /** @var Encoder\EncoderFactory $factory */
            $factory = $this->get('security.encoder_factory');
            $encoder = $factory->getEncoder($user);
            $password = $encoder->encodePassword($user->getPassword(), $user->getSalt());
            $user->setPassword($password);

            $em->persist($user);
            $em->flush();

            return $this->redirect($this->generateUrl('login'));

        }

        return $this->render(
            'IbwJobMBundle:Account:register.html.twig',
            array('form' => $form->createView())
        );
    }


    public function profileCandidateAction()
    {
        $id = $this->get('security.context')->getToken()->getUser()->getId();
        $em = $this->getDoctrine()->getManager();

//        $candidates = $em->getRepository('IbwJobMBundle:User')->getUserInfo($id);
        $candidates = $em->getRepository('IbwJobMBundle:Candidate')->getCandidateInfo($id);

        return $this->render('IbwJobMBundle:Account:profile_user.html.twig', array(
            'candidates' => $candidates,

        ));
    }


    public function profileCompanyAction()
    {
//        if ($this->get('security.context')->isGranted('ROLE_COMPANY') == false) {
//            //throw new AccessDeniedException();
//            return $this->render('IbwJobMBundle:Account:access_denied.html.twig');
//        }

        $em = $this->getDoctrine()->getManager();

        $companies = $em->getRepository('IbwJobMBundle:Company')->findAll();

        return $this->render('IbwJobMBundle:Account:profile_company.html.twig', array(
            'companies' => $companies
        ));
    }

    public function EditCandidateAction(Request $request)
    {
        $id = $this->get('security.context')->getToken()->getUser()->getId();

        /** @var EntityManagerInterface $em */
        $em = $this->getDoctrine()->getManager();
        $candidate = $em->getRepository('IbwJobMBundle:Candidate')->getCandidateInfo($id);
        $editForm = $this->createForm(new CandidateType(), $candidate);

        return $this->render('IbwJobMBundle:Account:edit_profile_candidate.html.twig', array(
            'candidate' => $candidate,
            'edit_form' => $editForm->createView(),
        ));
    }

    public function updateCandidateAction(Request $request)
    {
        $id = $this->get('security.context')->getToken()->getUser()->getId();
        $em = $this->getDoctrine()->getManager();

        $candidates = $em->getRepository('IbwJobMBundle:Candidate')->getCandidateInfo($id);

        $editForm = $this->createForm(new CandidateType(), $candidates);

        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($candidates);
            $em->flush();

            return $this->redirect($this->generateUrl('profile_candidate'));
        }

        return $this->redirect($this->generateUrl('profile_candidate', array(
            'candidates' => $candidates,

        )));
    }

    public function AddInfoCandidateAction(Request $request){

        $entity = new Candidate();
        $form = $this->createForm(new CandidateType(), $entity);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setUser($this->get('security.context')->getToken()->getUser());

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl(
                'profile_candidate'
            ));
        }
        return $this->render('IbwJobMBundle:Account:add_info_candidate.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

}