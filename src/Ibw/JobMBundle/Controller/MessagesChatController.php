<?php

namespace Ibw\JobMBundle\Controller;

use Ibw\JobMBundle\Entity\MessagesChat;
use Ibw\JobMBundle\Form\MessagesChatType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class MessagesChatController extends Controller
{


    public  function sendMessagesAction()
    {
        return $this->render('IbwJobMBundle:Messages:messages.html.twig', array(
//                'jobs' => $jobs,
//            'resume' => $resume
        ));
    }

    public  function individualMessagesAction(Request $request)
    {
        $entity  = new MessagesChat();

        $em = $this->getDoctrine()->getManager();
//        $id = $this->get('security.context')->getToken()->getUser()->getId();

        $messages = $em->getRepository('IbwJobMBundle:MessagesChat')->getMessagesByUser();
//        print_r($messages);die;


        $form = $this->createForm(new MessagesChatType(),$entity);
        $form->bind($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl(
                'individual_messages'
            ));
        }
        return $this->render('IbwJobMBundle:Messages:individual_message.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'messages' => $messages
//            'user' => $user
        ));
//        return $this->render('IbwJobMBundle:Messages:individual_message.html.twig', array(
////                'jobs' => $jobs,
////            'resume' => $resume
//        ));
    }

//    public function createMessagesAction()
//    {
//        $entity  = new MessagesChat();
//
//        $form = $this->createForm(new MessagesChatType(),$entity);
//
//        if ($form->isValid()) {
//
//            $em = $this->getDoctrine()->getManager();
//            $em->persist($entity);
//            $em->flush();
//
//            return $this->redirect($this->generateUrl(
//                'individual_messages'
//            ));
//        }
//        return $this->render('IbwJobMBundle:Messages:individual_message.html.twig', array(
//            'entity' => $entity,
//            'form'   => $form->createView(),
//        ));
//
//    }

    public function showMessageAction()
    {

        $em = $this->getDoctrine()->getManager();
//        $id = $this->get('security.context')->getToken()->getUser()->getId();

        $messages = $em->getRepository('IbwJobMBundle:MessagesChat')->getMessages();

//      print_r($messages);die;


        return $this->render('IbwJobMBundle:Messages:individual_message.html.twig', array(
            'messages' => $messages,

        ));
    }


}
