$(window).on('load', function () {
    $('.selectpicker').selectpicker({
        'selectedText': 'cat'
    });

    // $('.selectpicker').selectpicker('hide');
});

$(function () {
    $("#sortable").sortable();
    $("#sortable").disableSelection();
});

tinymce.init({
    selector: "textarea",
    forced_root_block: false
});

$(function () {
    $(":file").change(function () {
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = imageIsLoaded;
            reader.readAsDataURL(this.files[0]);
        }
    });
});

function imageIsLoaded(e) {
    $('#myImg').attr('src', e.target.result);
    document.getElementById('removebtn').style.display = 'block'
}


removeImg = function(){
    document.getElementById('removebtn').style.display = 'none';
    document.getElementById("cv_file").value = null;
    $('#myImg').attr('src', '/bundles/ibwjobm/images/clear.jpg');
};

addTabHeaders = function(val, valForId){
    var h1Sect = document.getElementById(valForId + "h");
    h1Sect.appendChild(document.createTextNode(val));
    var labelSect = document.getElementById(valForId + "l");
    labelSect.appendChild(document.createTextNode(val));

    if ($('#cv_new_section_1_name').val() == ''){
        $('#cv_new_section_1_name').val(val)
    } else if($('#cv_new_section_2_name').val() == '') {
        $('#cv_new_section_2_name').val(val);
    } else {
        $('#cv_new_section_3_name').val(val);
    }

};
var count = 0;
addAnother = function() {
    var ul = document.getElementById("sortable");
    var val = document.getElementById("new").value;
    var Maxelements = 3;
    if (count < Maxelements) {
        count++;
        var valForId = "new_section_" + count;
        var li = document.createElement("li");
        var a = document.createElement("a");
        var span = document.createElement("span");
        li.setAttribute("class", "ui-sortable-handle");
        a.setAttribute("href", "#" + valForId);
        a.setAttribute("role", "tab");
        a.setAttribute("data-toggle", "tab");
        span.setAttribute("class", "renameSection");
        a.appendChild(document.createTextNode(val));
        ul.appendChild(li);
        li.appendChild(a);
        li.appendChild(span);
        addTabHeaders(val, valForId);
        $('#new').val('');
    }
    if (count == Maxelements){
        var div = document.getElementById("moreInputs");
        div.removeAttribute("data-toggle");
        $('#moreInputs').hide();
    }
};

addAnotherEdit = function(){
    var ul = document.getElementById("sortable");
    var val = document.getElementById("new").value;
    var countEdit = $("#sortable li").length;

    if( countEdit == 6) {
        var valForId = "new_section_" + 1;
        var li = document.createElement("li");
        var a = document.createElement("a");
        var span = document.createElement("span");
        li.setAttribute("class", "ui-sortable-handle");
        a.setAttribute("href", "#" + valForId);
        a.setAttribute("role", "tab");
        a.setAttribute("data-toggle", "tab");
        span.setAttribute("class", "renameSection");
        a.appendChild(document.createTextNode(val));
        ul.appendChild(li);
        li.appendChild(a);
        li.appendChild(span);
        addTabHeaders(val, valForId);
        $('#new').val('');
    }
    if( countEdit == 7) {
        var valForId = "new_section_" + 2;
        var li = document.createElement("li");
        var a = document.createElement("a");
        var span = document.createElement("span");
        li.setAttribute("class", "ui-sortable-handle");
        a.setAttribute("href", "#" + valForId);
        a.setAttribute("role", "tab");
        a.setAttribute("data-toggle", "tab");
        span.setAttribute("class", "renameSection");
        a.appendChild(document.createTextNode(val));
        ul.appendChild(li);
        li.appendChild(a);
        li.appendChild(span);
        addTabHeaders(val, valForId);
        $('#new').val('');
    }
    if( countEdit == 8) {
        var valForId = "new_section_" + 3;
        var li = document.createElement("li");
        var a = document.createElement("a");
        var span = document.createElement("span");
        li.setAttribute("class", "ui-sortable-handle");
        a.setAttribute("href", "#" + valForId);
        a.setAttribute("role", "tab");
        a.setAttribute("data-toggle", "tab");
        span.setAttribute("class", "renameSection");
        a.appendChild(document.createTextNode(val));
        ul.appendChild(li);
        li.appendChild(a);
        li.appendChild(span);
        addTabHeaders(val, valForId);
        $('#new').val('');
    }

    if(countEdit == 8){
        var div = document.getElementById("moreInputsEdit");
        div.removeAttribute("data-toggle");
        $('#moreInputsEdit').hide();
    }
};

    $('#removeIdWork').show();
    $('#removeIdWorkNew').hide();
    $('#Workexperience-show').show();
    $('#Workexperience-show-new').hide();
    $('#addIdWork').show();
    $('#showWork').hide();

    $('#showWork').click(function () {
        $('#Workexperience-show').show();
        $('#showWork').hide();
        $('#removeIdWork').show();
        $('#addIdWork').show();
        //if('#Workexperience-show' && '#Workexperience-show-new'){
        //    $('#addIdWork').hide();
        //}
    });

    $('#addIdWork').click(function(){
        $('#Workexperience-show-new').show();
        $('#addIdWork').hide();
        $('#removeIdWorkNew').show();
        $('#showWork').hide();
        $('#removeIdWork').show();
    });

    $('#removeIdWork').click(function () {
        $('#showWork').show();
        $('#removeIdWork').hide();
        $('#removeIdWorkNew').show();

        var workDiv = $('#Workexperience-show');
        workDiv.hide();
        workDiv.children().find('input').each(function () {
            $(this).val('');

        });
    });

    $('#removeIdWorkNew').click(function () {
        $('#addIdWork').show();
        $('#removeIdWorkNew').show();

        var workDivNew = $('#Workexperience-show-new');
        workDivNew.hide();
        workDivNew.children().find('input').each(function () {
            $(this).val('');

        });
    });

    $('#removeIdEdu').show();
    $('#removeIdEduNew').hide();
    $('#Education-show').show();
    $('#Education-show-new').hide();
    $('#addIdEdu').show();
    $('#showEdu').hide();


    $('#showEdu').click(function () {
        $('#Education-show').show();
        $('#showEdu').hide();
        $('#removeIdEdu').show();
        $('#addIdEdu').show();
    });

    $('#addIdEdu').click(function(){
        $('#Education-show-new').show();
        $('#addIdEdu').hide();
        $('#removeIdEduNew').show();
        $('#showEdu').hide();
    });


    $('#removeIdEdu').click(function () {
        $('#showEdu').show();
        $('#removeIdEdu').hide();
        $('#removeIdEduNew').hide();

        var eduDiv = $('#Education-show');
        eduDiv.hide();
        eduDiv.children().find('input').each(function () {
            $(this).val('');
        });
    });

    $('#removeIdEduNew').click(function () {
        $('#addIdEdu').show();
        $('#removeIdEduNew').hide();

        var eduDivNew = $('#Education-show-new');
        eduDivNew.hide();
        eduDivNew.children().find('input').each(function () {
            $(this).val('');
        });
    });


$(function() {
    $("#addDateInModal").click(function() {
        //            var photo = $("#photo");
        var name = $("#title");
        var email = $("#cv_emailModal");
        var website = $("#cv_websitesModal");
        var phone = $("#cv_phoneModal");
        var addressStreet = $("#cv_addressstreetModal");
        var addressCity = $("#cv_addresscityModal");
        var addressCountry = $("#cv_addresscountryModal");
        var companyname = $("#cv_companynameModal");
        var startwork = $("#cv_startdateworkModal");
        var endwork = $("cv_enddateworkModal");
        var jobtitle = $("#cv_jobtitleModal");
        var otherinformwork = $("#cv_otherinformationworkModal");
        var companynamenew = $("#cv_companynamenewModal");
        var startworknew = $("#cv_startdateworknewModal");
        var endworknew = $("cv_enddateworknewModal");
        var jobtitlenew = $("#cv_jobtitlenewModal");
        var otherinformworknew = $("#cv_otherinformationworknewModal");
        var qualifications = $("#cv_qualificationsModal");
        var specialization = $("#cv_specializationModal");
        var startlearn = $("#cv_startdatelearnModal");
        var endlearn = $("#cv_enddatelearnModal");
        var institutionname = $("#cv_institutionnameModal");
        var otherinfoedu = $("#cv_otherinformationeducationModal");
        var specializationnew = $("#cv_specializationnewModal");
        var startlearnnew = $("#cv_startdatelearnnewModal");
        var endlearnnew = $("#cv_enddatelearnnewModal");
        var institutionnamenew = $("#cv_institutionnamenewModal");
        var otherinfoedunew = $("#cv_otherinformationeducationnewModal");
        var skills = $("#cv_skillsModal");
        var competence = $("#cv_competenceModal");
        var newsec1 = $("#cv_newsection1Modal");
        var newsec2 = $("#cv_newsection2Modal");
        var newsec3 = $("#cv_newsection3Modal");
//              photo.empty();
        name.empty();
        email.empty();
        website.empty();
        phone.empty();
        addressStreet.empty();
        addressCity.empty();
        addressCountry.empty();
        companyname.empty();
        startwork.empty();
        endwork.empty();
        jobtitle.empty();
        otherinformwork.empty();
        companynamenew.empty();
        startworknew.empty();
        endworknew.empty();
        jobtitlenew.empty();
        otherinformworknew.empty();
        qualifications.empty();
        specialization.empty();
        startlearn.empty();
        endlearn.empty();
        institutionname.empty();
        otherinfoedu.empty();
        specializationnew.empty();
        startlearnnew.empty();
        endlearnnew.empty();
        institutionnamenew.empty();
        otherinfoedunew.empty();
        skills.empty();
        competence.empty();
        newsec1.empty();
        newsec2.empty();
        newsec3.empty();
//              photo.append($("#myImg"))

        name.append($("#cv_name").val());

        if ($('#cv_email').val()){
            $('#appendEmail').show();
            email.append($('#cv_email').val())
        }
        else
        {
            $('#appendEmail').hide();
        }

        if ($('#cv_websites').val()){
            $('#appendWebsite').show();
            website.append($("#cv_websites").val());
        }
        else
        {
            $('#appendWebsite').hide();
        }

        if ($('#cv_phone').val()){
            $('#appendPhone').show();
            phone.append($("#cv_phone").val());
        }
        else
        {
            $('#appendPhone').hide();
        }

        if ($('#cv_street').val()){
            $('#appendAddress').show();
            addressStreet.append($("#cv_street").val());
        }
        else
        {
            $('#appendAddress').hide();
        }

        if (($("#cv_city").val()))
        {
            $('#appendAddress').show();
            addressCity.append($("#cv_city").val());
        }
        else
        {
            $('#appendAddress').hide();
        }

        if ($("#cv_country").val())
        {
            $('#appendAddress').show();
            addressCountry.append($("#cv_country").val());
        }
        else
        {
            $('#appendAddress').hide();
        }


        if ($('#cv_company_name').val())
        {
            companyname.append($("#cv_company_name").val());
        }

        if ($('#cv_start_date_work').val())
        {
            startwork.append($("#cv_start_date_work").val());
        }

        if ($('#cv_end_date_work').val())
        {
            endwork.append($("#cv_end_date_work").val());
        }

        if ($('#cv_job_title').val())
        {
            jobtitle.append($("#cv_job_title").val());
        }

        if ($('#cv_other_information_work').val())
        {
            otherinformwork.append($("#cv_other_information_work").val());
        }

        if ($('#cv_company_name_new').val())
        {
            companynamenew.append($("#cv_company_name_new").val());
        }

        if ($('#cv_start_date_work_new').val())
        {
            startworknew.append($("#cv_start_date_work_new").val());
        }

        if ($('#cv_end_date_work_new').val())
        {
            endworknew.append($("#cv_end_date_work_new").val());
        }

        if ($('#cv_job_title_new').val())
        {
            jobtitlenew.append($("#cv_job_title_new").val());
        }

        if ($('#cv_other_information_work_new').val())
        {
            otherinformworknew.append($("#cv_other_information_work_new").val());
        }

        if ($("#cv_qualifications").val())
        {
            $('#appendQualifications').show();
            qualifications.append($("#cv_qualifications").val());
        }
        else
        {
            $('#appendQualifications').hide();
        }


        if ($('#cv_specialization').val())
        {
            specialization.append($("#cv_specialization").val());
        }

        if ($('#cv_start_date_learn').val())
        {
            startlearn.append($("#cv_start_date_learn").val());
        }

        if ($('#cv_end_date_learn').val())
        {
            endlearn.append($("#cv_end_date_learn").val());
        }

        if ($('#cv_institution_name').val())
        {
            institutionname.append($("#cv_institution_name").val());
        }

        if ($('#cv_other_information_education').val())
        {
            otherinfoedu.append($("#cv_other_information_education").val());
        }

        if ($('#cv_specialization_new').val())
        {
            specializationnew.append($("#cv_specialization_new").val());
        }

        if ($('#cv_start_date_learn_new').val())
        {
            startlearnnew.append($("#cv_start_date_learn_new").val());
        }

        if ($('#cv_end_date_learn_new').val())
        {
            endlearnnew.append($("#cv_end_date_learn_new").val());
        }

        if ($('#cv_institution_name_new').val())
        {
            institutionnamenew.append($("#cv_institution_name_new").val());
        }

        if ($('#cv_other_information_education_new').val())
        {
            otherinfoedunew.append($("#cv_other_information_education").val());
        }


        if ($("#cv_skills").val())
        {
            $('#appendSkills').show();
            skills.append($("#cv_skills").val());
        }
        else
        {
            $('#appendSkills').hide();
        }

        if ($("#cv_competence").val())
        {
            $('#appendCompetence').show();
            competence.append($("#cv_competence").val());
        }
        else
        {
            $('#appendCompetence').hide();
        }

        if ($("#cv_new_section_1").val())
        {
            $('#appendSection1').show();
            newsec1.append($("#cv_new_section_1").val());
        }
        else
        {
            $('#appendSection1').hide();
        }


        if ($("#cv_new_section_2").val())
        {
            $('#appendSection2').show();
            newsec2.append($("#cv_new_section_2").val());
        }
        else
        {
            $('#appendSection2').hide();
        }

        if ($("#cv_new_section_3").val())
        {
            $('#appendSection3').show();
            newsec3.append($("#cv_new_section_3").val());
        }
        else
        {
            $('#appendSection3').hide();
        }

    })
});
/*function on edit cv page*/


    $('#removeIdWorkEdit').show();
    $('#removeIdWorkNewEdit').hide();
    $('#Workexperience-show-edit').show();
    $('#Workexperience-show-new-edit').hide();
    $('#addIdWorkEdit').show();
    $('#showWorkEdit').hide();

    $('#showWorkEdit').click(function () {
        $('#Workexperience-show-edit').show();
        $('#showWorkEdit').hide();
        $('#removeIdWorkEdit').show();
        $('#addIdWorkEdit').show();
    });

    $('#addIdWorkEdit').click(function(){
        $('#Workexperience-show-new-edit').show();
        $('#addIdWorkEdit').hide();
        $('#removeIdWorkNewEdit').show();
        $('#showWorkEdit').hide();
    });

    $('#removeIdWorkEdit').click(function () {
        $('#showWorkEdit').show();
        $('#removeIdWorkEdit').hide();
        $('#removeIdWorkNewEdit').hide();

        var workDivEdit = $('#Workexperience-show-edit');
        workDivEdit.hide();
        workDivEdit.children().find('input').each(function () {
            $(this).val('');

        });
    });

    $('#removeIdWorkNewEdit').click(function () {
        $('#addIdWorkEdit').show();
        $('#removeIdWorkNewEdit').show();

        var workDivNewEdit = $('#Workexperience-show-new-edit');
        workDivNewEdit.hide();
        workDivNewEdit.children().find('input').each(function () {
            $(this).val('');

        });
    });

    $('#removeIdEduEdit').show();
    $('#removeIdEduNewEdit').hide();
    $('#Education-show-edit').show();
    $('#Education-show-new-edit').hide();
    $('#addIdEduEdit').show();
    $('#showEduEdit').hide();


    $('#showEduEdit').click(function () {
        $('#Education-show-edit').show();
        $('#showEduEdit').hide();
        $('#removeIdEduEdit').show();
        $('#addIdEduEdit').show();
    });

    $('#addIdEduEdit').click(function(){
        $('#Education-show-new-edit').show();
        $('#addIdEduEdit').hide();
        $('#removeIdEduNewEdit').show();
        $('#showEduEdit').hide();
    });


    $('#removeIdEduEdit').click(function () {
        $('#showEduEdit').show();
        $('#removeIdEduEdit').hide();
        $('#removeIdEduNewEdit').hide();

        var eduDivEdit = $('#Education-show-edit');
        eduDivEdit.hide();
        eduDivEdit.children().find('input').each(function () {
            $(this).val('');
        });
    });

    $('#removeIdEduNewEdit').click(function () {
        $('#addIdEduEdit').show();
        $('#removeIdEduNewEdit').hide();

        var eduDivNewEdit = $('#Education-show-new-edit');
        eduDivNewEdit.hide();
        eduDivNewEdit.children().find('input').each(function () {
            $(this).val('');
        });
    });


$(function() {
    $("#PreviewDateInModal").click(function() {
//            var photo = $("#photo");
        var name = $("#title");
        var email = $("#cv_emailModal");
        var website = $("#cv_websitesModal");
        var phone = $("#cv_phoneModal");
        var addressStreet = $("#cv_addressstreetModal");
        var addressCity = $("#cv_addresscityModal");
        var addressCountry = $("#cv_addresscountryModal");
        var companyname = $("#cv_companynameModal");
        var startwork = $("#cv_startdateworkModal");
        var endwork = $("cv_enddateworkModal");
        var jobtitle = $("#cv_jobtitleModal");
        var otherinformwork = $("#cv_otherinformationworkModal");
        var companynamenew = $("#cv_companynamenewModal");
        var startworknew = $("#cv_startdateworknewModal");
        var endworknew = $("cv_enddateworknewModal");
        var jobtitlenew = $("#cv_jobtitlenewModal");
        var otherinformworknew = $("#cv_otherinformationworknewModal");
        var qualifications = $("#cv_qualificationsModal");
        var specialization = $("#cv_specializationModal");
        var starlearn = $("#cv_startdatelearnModal");
        var endlearn = $("#cv_enddatelearnModal");
        var institutionname = $("#cv_institutionnameModal");
        var otherinfoedu = $("#cv_otherinformationeducationModal");
        var specializationnew = $("#cv_specializationnewModal");
        var starlearnnew = $("#cv_startdatelearnnewModal");
        var endlearnnew = $("#cv_enddatelearnnewModal");
        var institutionnamenew = $("#cv_institutionnamenewModal");
        var otherinfoedunew = $("#cv_otherinformationeducationnewModal");
        var skills = $("#cv_skillsModal");
        var competence = $("#cv_competenceModal");
        var newsec1 = $("#cv_newsection1Modal");
        var newsec2 = $("#cv_newsection2Modal");
        var newsec3 = $("#cv_newsection3Modal");
//            photo.empty();
        name.empty();
        email.empty();
        website.empty();
        phone.empty();
        addressStreet.empty();
        addressCity.empty();
        addressCountry.empty();
        companyname.empty();
        startwork.empty();
        endwork.empty();
        jobtitle.empty();
        otherinformwork.empty();
        companynamenew.empty();
        startworknew.empty();
        endworknew.empty();
        jobtitlenew.empty();
        otherinformworknew.empty();
        qualifications.empty();
        specialization.empty();
        starlearn.empty();
        endlearn.empty();
        institutionname.empty();
        otherinfoedu.empty();
        specializationnew.empty();
        starlearnnew.empty();
        endlearnnew.empty();
        institutionnamenew.empty();
        otherinfoedunew.empty();
        skills.empty();
        competence.empty();
        newsec1.empty();
        newsec2.empty();
        newsec3.empty();
//            photo.append($("#myImg"))
        name.append($("#cv_name").val());
        email.append($("#cv_email").val());
        website.append($("#cv_websites").val());
        phone.append($("#cv_phone").val());
        addressStreet.append($("#cv_street").val());
        addressCity.append($("#cv_city").val());
        addressCountry.append($("#cv_country").val());
        companyname.append($("#cv_company_name").val());
        startwork.append($("#cv_start_date_work").val());
        endwork.append($("#cv_end_date_work").val());
        jobtitle.append($("#cv_job_title").val());
        otherinformwork.append($("#cv_other_information_work").val());
        companynamenew.append($("#cv_company_name_new").val());
        startworknew.append($("#cv_start_date_work_new").val());
        endworknew.append($("#cv_end_date_work_new").val());
        jobtitlenew.append($("#cv_job_title_new").val());
        otherinformworknew.append($("#cv_other_information_work_new").val());
        qualifications.append($("#cv_qualifications").val());
        specialization.append($("#cv_specialization").val());
        starlearn.append($("#cv_start_date_learn").val());
        endlearn.append($("#cv_end_date_learn").val());
        institutionname.append($("#cv_institution_name").val());
        otherinfoedu.append($("#cv_other_information_education").val());
        specializationnew.append($("#cv_specialization_new").val());
        starlearnnew.append($("#cv_start_date_learn-new").val());
        endlearnnew.append($("#cv_end_date_learn-new").val());
        institutionnamenew.append($("#cv_institution_name-new").val());
        otherinfoedunew.append($("#cv_other_information_education-new").val());
        skills.append($("#cv_skills").val());
        competence.append($("#cv_competence").val());
        newsec1.append($("#cv_new_section_1").val());
        newsec2.append($("#cv_new_section_2").val());
        newsec3.append($("#cv_new_section_3").val());
    })
});